CREATE DATABASE  IF NOT EXISTS `tspms` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `tspms`;
-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: aa4xtipon1te5t.c7lenwvlyctz.ap-southeast-1.rds.amazonaws.com    Database: tspms
-- ------------------------------------------------------
-- Server version	5.6.27-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `actor`
--

DROP TABLE IF EXISTS `actor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actor` (
  `actor_id` int(11) NOT NULL AUTO_INCREMENT,
  `actor_name` varchar(250) NOT NULL,
  `project_id` int(11) NOT NULL,
  `classification` varchar(500) NOT NULL,
  `description` longtext,
  `last_updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sub_id` int(11) NOT NULL,
  PRIMARY KEY (`actor_id`),
  UNIQUE KEY `unique_actor_id` (`actor_id`),
  KEY `project_id` (`project_id`),
  CONSTRAINT `actor_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`project_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actor`
--

LOCK TABLES `actor` WRITE;
/*!40000 ALTER TABLE `actor` DISABLE KEYS */;
/*!40000 ALTER TABLE `actor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `c_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(20) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `company_name` varchar(200) NOT NULL,
  `password_hash` varchar(200) NOT NULL,
  `hp_number` varchar(20) NOT NULL,
  `other_number` varchar(20) DEFAULT NULL,
  `email` varchar(200) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `last_updated` timestamp NOT NULL,
  `username` varchar(50) NOT NULL,
  PRIMARY KEY (`c_id`),
  KEY `c_id` (`c_id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dev_project`
--

DROP TABLE IF EXISTS `dev_project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dev_project` (
  `project_id` int(11) NOT NULL,
  `dev_id` int(11) NOT NULL,
  `last_updated` timestamp NOT NULL,
  PRIMARY KEY (`project_id`),
  UNIQUE KEY `unique_project_id` (`project_id`),
  KEY `dev_id` (`dev_id`),
  CONSTRAINT `dev_project_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`project_id`),
  CONSTRAINT `dev_project_ibfk_2` FOREIGN KEY (`dev_id`) REFERENCES `internal_user` (`u_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dev_project`
--

LOCK TABLES `dev_project` WRITE;
/*!40000 ALTER TABLE `dev_project` DISABLE KEYS */;
/*!40000 ALTER TABLE `dev_project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `environmental_factor`
--

DROP TABLE IF EXISTS `environmental_factor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `environmental_factor` (
  `Factor_ID` varchar(50) NOT NULL,
  `Description` varchar(250) NOT NULL,
  `Weight` double NOT NULL,
  `assigned_value` int(11) DEFAULT NULL,
  `type` varchar(20) NOT NULL,
  PRIMARY KEY (`Factor_ID`),
  UNIQUE KEY `unique_Factor_ID` (`Factor_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `environmental_factor`
--

LOCK TABLES `environmental_factor` WRITE;
/*!40000 ALTER TABLE `environmental_factor` DISABLE KEYS */;
INSERT INTO `environmental_factor` VALUES ('E1','Familiarity_with_development_process',1.5,3,'UCP'),('E2','Application_experience',0.5,4,'UCP'),('E3','Object_oriented_experience_of_team',1,2,'UCP'),('E4','Lead_analyst_capability',0.5,4,'UCP'),('E5','Motivation_of_the_team',1,2,'UCP'),('E6','Stability_of_requirements',2,1,'UCP'),('F7','Part_Time_staff',-1,1,'UCP'),('F8','Working_Hour',0,8,'Schedule');
/*!40000 ALTER TABLE `environmental_factor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `internal_user`
--

DROP TABLE IF EXISTS `internal_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `internal_user` (
  `u_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password_hash` varchar(200) DEFAULT NULL,
  `bb_username` varchar(200) DEFAULT NULL,
  `type` varchar(50) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `last_updated` timestamp NULL DEFAULT NULL,
  `bb_oauth_key` varchar(25) DEFAULT NULL,
  `bb_oauth_secret` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`u_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `internal_user`
--

LOCK TABLES `internal_user` WRITE;
/*!40000 ALTER TABLE `internal_user` DISABLE KEYS */;
INSERT INTO `internal_user` VALUES (8,'Wil','wil_ng','$2y$10$6HKcwKvAYWuSB/Y6SrdJwebU7k8CQL25THQT7BHAyePWE7bU1xtZW','wilng2005','Developer',1,'2016-02-12 15:46:11','fTfqBqLQ6fCzhVPMe2','eGQUgj4r76u2qh6XAQrbUNDs2h9TY2ah','wil_ng@theshipyard.sg'),(24,'Andrew','andrew','$2y$10$SdRhcEWh6SJCcRA7OleBtuG6MJ0Vk7V9kT6CWTK91odvYLN2vi03y','wilng2005','PM',1,'2016-02-17 20:52:05','fTfqBqLQ6fCzhVPMe2','eGQUgj4r76u2qh6XAQrbUNDs2h9TY2ah','andrew@theshipyard.sg');
/*!40000 ALTER TABLE `internal_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `issue_log`
--

DROP TABLE IF EXISTS `issue_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `issue_log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `issue_id` int(11) DEFAULT NULL,
  `repo_slug` varchar(100) NOT NULL,
  `title` varchar(80) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `date_updated` timestamp NOT NULL,
  `updated_by` varchar(100) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `assignee` int(11) NOT NULL,
  `workflow` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `issue_log`
--

LOCK TABLES `issue_log` WRITE;
/*!40000 ALTER TABLE `issue_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `issue_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `issue_report`
--

DROP TABLE IF EXISTS `issue_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `issue_report` (
  `issue_id` int(11) NOT NULL AUTO_INCREMENT,
  `local_id` int(11) DEFAULT NULL,
  `project_id` int(11) NOT NULL,
  `title` varchar(200) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `status` varchar(30) DEFAULT NULL,
  `workflow` varchar(30) DEFAULT NULL,
  `duration_1` int(10) unsigned zerofill DEFAULT NULL,
  `duration_2` int(10) unsigned zerofill DEFAULT NULL,
  `duration_3` int(10) unsigned zerofill DEFAULT NULL,
  `duration_4` int(10) unsigned zerofill DEFAULT NULL,
  `duration_5` int(10) unsigned zerofill DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  `date_resolved` timestamp NULL DEFAULT NULL,
  `date_due` date DEFAULT NULL,
  `phase` int(11) DEFAULT NULL,
  `date_loaded` timestamp NULL DEFAULT NULL,
  `kind` varchar(30) NOT NULL,
  `expected_duration` int(11) DEFAULT NULL,
  `actual_duration` int(11) DEFAULT NULL,
  PRIMARY KEY (`issue_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27962 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `issue_report`
--

LOCK TABLES `issue_report` WRITE;
/*!40000 ALTER TABLE `issue_report` DISABLE KEYS */;
/*!40000 ALTER TABLE `issue_report` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message` (
  `message_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `pm_id` int(11) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `to_pm` tinyint(4) DEFAULT NULL,
  `body` text,
  `is_file` text,
  `seen` tinyint(4) DEFAULT NULL,
  `time_created` int(11) NOT NULL,
  PRIMARY KEY (`message_id`),
  UNIQUE KEY `unique_message_id` (`message_id`),
  KEY `customer_id` (`customer_id`),
  KEY `pm_id` (`pm_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1082 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message`
--

LOCK TABLES `message` WRITE;
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
/*!40000 ALTER TABLE `message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `milestone`
--

DROP TABLE IF EXISTS `milestone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `milestone` (
  `milestone_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `deadline` date NOT NULL,
  `if_completed` tinyint(4) NOT NULL,
  `bb_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`milestone_id`),
  UNIQUE KEY `unique_milestone_id` (`milestone_id`),
  KEY `post_id` (`post_id`),
  CONSTRAINT `milestone_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `post` (`post_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=139 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `milestone`
--

LOCK TABLES `milestone` WRITE;
/*!40000 ALTER TABLE `milestone` DISABLE KEYS */;
/*!40000 ALTER TABLE `milestone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notification`
--

DROP TABLE IF EXISTS `notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification` (
  `notification_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `change_type` varchar(50) NOT NULL,
  `post_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `redirect` varchar(50) NOT NULL,
  `created_datetime` timestamp NULL DEFAULT NULL,
  `created_by` varchar(50) NOT NULL,
  PRIMARY KEY (`notification_id`),
  UNIQUE KEY `unique_notification_id` (`notification_id`),
  KEY `project_id` (`project_id`),
  KEY `post_id` (`post_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `notification_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`project_id`) ON DELETE CASCADE,
  CONSTRAINT `notification_ibfk_3` FOREIGN KEY (`post_id`) REFERENCES `post` (`post_id`) ON DELETE CASCADE,
  CONSTRAINT `notification_ibfk_4` FOREIGN KEY (`user_id`) REFERENCES `internal_user` (`u_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1877 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notification`
--

LOCK TABLES `notification` WRITE;
/*!40000 ALTER TABLE `notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_token`
--

DROP TABLE IF EXISTS `oauth_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_token` (
  `user_id` int(11) NOT NULL,
  `token` varchar(200) DEFAULT NULL,
  `ttl` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_token`
--

LOCK TABLES `oauth_token` WRITE;
/*!40000 ALTER TABLE `oauth_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `online_user`
--

DROP TABLE IF EXISTS `online_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `online_user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `user_type` varchar(11) DEFAULT NULL,
  `timestamp` int(11) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `online_user`
--

LOCK TABLES `online_user` WRITE;
/*!40000 ALTER TABLE `online_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `online_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phase`
--

DROP TABLE IF EXISTS `phase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phase` (
  `phase_id` int(11) NOT NULL,
  `phase_name` varchar(50) NOT NULL,
  PRIMARY KEY (`phase_id`),
  UNIQUE KEY `unique_phase_id` (`phase_id`),
  UNIQUE KEY `unique_phase_name` (`phase_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phase`
--

LOCK TABLES `phase` WRITE;
/*!40000 ALTER TABLE `phase` DISABLE KEYS */;
INSERT INTO `phase` VALUES (3,'Build'),(5,'Deploy'),(1,'Lead'),(2,'Requirement'),(4,'Testing');
/*!40000 ALTER TABLE `phase` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post`
--

DROP TABLE IF EXISTS `post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post` (
  `post_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_phase_id` int(11) NOT NULL,
  `header` varchar(225) NOT NULL,
  `body` text,
  `datetime_created` timestamp NOT NULL,
  `type` varchar(50) NOT NULL,
  `last_updated` timestamp NOT NULL,
  PRIMARY KEY (`post_id`),
  UNIQUE KEY `unique_post_id` (`post_id`),
  KEY `project_phase_id` (`project_phase_id`),
  CONSTRAINT `post_ibfk_1` FOREIGN KEY (`project_phase_id`) REFERENCES `project_phase` (`project_phase_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=343 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post`
--

LOCK TABLES `post` WRITE;
/*!40000 ALTER TABLE `post` DISABLE KEYS */;
/*!40000 ALTER TABLE `post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project` (
  `project_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_title` varchar(100) NOT NULL,
  `project_description` varchar(100) DEFAULT NULL,
  `tags` varchar(225) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `start_time` date NOT NULL,
  `staging_link` varchar(256) DEFAULT NULL,
  `production_link` varchar(256) DEFAULT NULL,
  `customer_preview_link` varchar(256) DEFAULT NULL,
  `current_project_phase_id` int(10) NOT NULL,
  `file_repo_name` varchar(225) DEFAULT NULL,
  `bitbucket_repo_name` varchar(225) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `project_value` varchar(225) DEFAULT NULL,
  `last_updated` timestamp NOT NULL,
  `is_ongoing` tinyint(4) NOT NULL DEFAULT '1',
  `c_id` int(11) NOT NULL,
  `pm_id` int(11) DEFAULT NULL,
  `repo_name_valid` tinyint(4) NOT NULL DEFAULT '0',
  `issue_count` int(11) DEFAULT NULL,
  `project_code` varchar(8) DEFAULT NULL,
  `distributed_system` int(11) DEFAULT NULL,
  `response_time` int(11) DEFAULT NULL,
  `end-user_efficiency` int(11) DEFAULT NULL,
  `internal_processing_complexity` int(11) DEFAULT NULL,
  `code_reusability` int(11) DEFAULT NULL,
  `easy_to_install` int(11) DEFAULT NULL,
  `easy_to_use` int(11) DEFAULT NULL,
  `portability_to_other_platforms` int(11) DEFAULT NULL,
  `system_maintenance` int(11) DEFAULT NULL,
  `concurrent_processing` int(11) DEFAULT NULL,
  `security_features` int(11) DEFAULT NULL,
  `access_for_third_parties` int(11) DEFAULT NULL,
  `end_user_training` int(11) DEFAULT NULL,
  PRIMARY KEY (`project_id`),
  KEY `c_id` (`c_id`),
  KEY `pm_id` (`pm_id`),
  CONSTRAINT `project_ibfk_1` FOREIGN KEY (`c_id`) REFERENCES `customer` (`c_id`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project`
--

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_phase`
--

DROP TABLE IF EXISTS `project_phase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_phase` (
  `project_phase_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `phase_id` int(11) NOT NULL,
  `start_time` date DEFAULT NULL,
  `end_time` date DEFAULT NULL,
  `estimated_end_time` date DEFAULT NULL,
  `last_updated` timestamp NOT NULL,
  PRIMARY KEY (`project_phase_id`),
  UNIQUE KEY `unique_project_phase_id` (`project_phase_id`),
  KEY `project_id` (`project_id`),
  KEY `phase_id` (`phase_id`),
  CONSTRAINT `project_phase_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`project_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `project_phase_ibfk_2` FOREIGN KEY (`phase_id`) REFERENCES `phase` (`phase_id`)
) ENGINE=InnoDB AUTO_INCREMENT=202 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_phase`
--

LOCK TABLES `project_phase` WRITE;
/*!40000 ALTER TABLE `project_phase` DISABLE KEYS */;
/*!40000 ALTER TABLE `project_phase` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schedule`
--

DROP TABLE IF EXISTS `schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schedule` (
  `schedule_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `date_time` date NOT NULL,
  `hour_planned` double NOT NULL,
  PRIMARY KEY (`schedule_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17054 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schedule`
--

LOCK TABLES `schedule` WRITE;
/*!40000 ALTER TABLE `schedule` DISABLE KEYS */;
/*!40000 ALTER TABLE `schedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `task`
--

DROP TABLE IF EXISTS `task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `task` (
  `task_id` int(11) NOT NULL AUTO_INCREMENT,
  `targeted_start_datetime` timestamp NOT NULL,
  `targeted_end_datetime` timestamp NOT NULL,
  `start_datetime` timestamp NULL DEFAULT NULL,
  `end_datetime` timestamp NULL DEFAULT NULL,
  `importance` int(11) DEFAULT NULL,
  `datetime_created` timestamp NULL DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT NULL,
  `if_completed` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `content` varchar(225) NOT NULL,
  `phase_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`task_id`),
  UNIQUE KEY `unique_task_id` (`task_id`),
  KEY `project_id` (`project_id`),
  CONSTRAINT `task_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `task`
--

LOCK TABLES `task` WRITE;
/*!40000 ALTER TABLE `task` DISABLE KEYS */;
/*!40000 ALTER TABLE `task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `updates`
--

DROP TABLE IF EXISTS `updates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `updates` (
  `update_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `posted_by` varchar(50) NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`update_id`),
  UNIQUE KEY `unique_update_id` (`update_id`),
  KEY `post_id` (`post_id`),
  CONSTRAINT `updates_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `post` (`post_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=175 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `updates`
--

LOCK TABLES `updates` WRITE;
/*!40000 ALTER TABLE `updates` DISABLE KEYS */;
/*!40000 ALTER TABLE `updates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `upload`
--

DROP TABLE IF EXISTS `upload`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `upload` (
  `fid` int(11) NOT NULL AUTO_INCREMENT,
  `file_url` varchar(512) NOT NULL,
  `file_key` varchar(512) NOT NULL DEFAULT '',
  `filename` varchar(512) NOT NULL,
  `last_updated` datetime DEFAULT NULL,
  `pid` int(11) NOT NULL,
  `file_type` varchar(30) NOT NULL DEFAULT '',
  `parent_id` varchar(512) NOT NULL DEFAULT '',
  `index` int(11) NOT NULL,
  `file_size` int(11) NOT NULL,
  PRIMARY KEY (`fid`)
) ENGINE=InnoDB AUTO_INCREMENT=221 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `upload`
--

LOCK TABLES `upload` WRITE;
/*!40000 ALTER TABLE `upload` DISABLE KEYS */;
/*!40000 ALTER TABLE `upload` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `urgency_score_record`
--

DROP TABLE IF EXISTS `urgency_score_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `urgency_score_record` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `score` float NOT NULL,
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `urgency_score_record`
--

LOCK TABLES `urgency_score_record` WRITE;
/*!40000 ALTER TABLE `urgency_score_record` DISABLE KEYS */;
/*!40000 ALTER TABLE `urgency_score_record` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `use_case`
--

DROP TABLE IF EXISTS `use_case`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `use_case` (
  `usecase_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `sub_id` int(11) NOT NULL,
  `title` mediumtext NOT NULL,
  `flow` longtext NOT NULL,
  `classification` varchar(50) DEFAULT NULL,
  `stakeholders` tinytext NOT NULL,
  `type` varchar(50) NOT NULL,
  `last_updated` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`usecase_id`),
  KEY `project_id` (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `use_case`
--

LOCK TABLES `use_case` WRITE;
/*!40000 ALTER TABLE `use_case` DISABLE KEYS */;
/*!40000 ALTER TABLE `use_case` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userlog`
--

DROP TABLE IF EXISTS `userlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userlog` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `u_id` int(11) NOT NULL,
  `action` varchar(225) NOT NULL,
  `timestamp` timestamp NOT NULL,
  PRIMARY KEY (`log_id`),
  KEY `u_id` (`u_id`),
  CONSTRAINT `userlog_ibfk_1` FOREIGN KEY (`u_id`) REFERENCES `internal_user` (`u_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userlog`
--

LOCK TABLES `userlog` WRITE;
/*!40000 ALTER TABLE `userlog` DISABLE KEYS */;
/*!40000 ALTER TABLE `userlog` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-04-21 18:20:12
