from __future__ import print_function

#from datetime import datetime
from urllib2 import urlopen

server = 'http://52.76.235.20'
paths = [
    '/issues/store_issue_urgency_score_across_projects',
    '/scheduled_tasks/calibrate_bb_milestones',
    '/dashboard/fetch_all_issues'
]


'''
def validate(res):
    Return False to trigger the canary

    Currently this simply checks whether the EXPECTED string is present.
    However, you could modify this to perform any number of arbitrary
    checks on the contents of SITE.
    
    return EXPECTED in res
'''

def lambda_handler(event, context):
    # print('Checking {} at {}...'.format(SITE, event['time']))
    for path in paths:
        try:
            urlopen(server+path).read()
        except :
            print('Exception encountered')
            raise
    #finally:
        # print('Check complete at {}'.format(str(datetime.now())))