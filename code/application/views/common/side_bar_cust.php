<?php
$_lb_active= $_lb_active;
$project_id=$project_id;
?>

<aside class="sidebar-left">
    <div class="sidebar-links">
        <a class="link-blue <?=1==$_lb_active?" selected":""?>" href="<?=base_url().'projects/customer_view/'.$project_id?>">
            <div class="sidebar-links-inner"><i class="fa fa-flag"></i><span class="nav-text">Update & Milestone</span></div>
        </a>
        <a class="link-blue <?=2==$_lb_active?" selected":""?>" href="<?=base_url().'Usecases/customer_usecases/'.$project_id?>">
            <div class="sidebar-links-inner"><i class="fa fa-list"></i><span class="nav-text">Use Case List</span></div>
        </a>
        <a class="link-blue  <?=3==$_lb_active?" selected":""?>" href="<?=base_url().'Upload/customer_repo/'.$project_id?>">
            <div class="sidebar-links-inner"><i class="fa fa-folder"></i><span class="nav-text">File Repository</span></div>
        </a>
    </div>
</aside>