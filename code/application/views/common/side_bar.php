<?php
$_lb_active= $_lb_active;
?>

<aside class="sidebar-left">
    <div class="sidebar-links">
        <a class="link-blue <?=1==$_lb_active?" selected":""?>" href="<?=base_url().'Projects/view_dashboard/'.$project["project_id"]?>">
            <div class="sidebar-links-inner"><i class="fa fa-tasks"></i><span class="nav-text">Project Overview</span></div>
        </a>
        <a class="link-blue <?=2==$_lb_active?" selected":""?>" href="<?=base_url().'Projects/view_updates/'.$project["project_id"]?>">
            <div class="sidebar-links-inner"><i class="fa fa-flag"></i><span class="nav-text">Update & Milestone</span></div>
        </a>
        <?php
        if($project['bitbucket_repo_name']==null):?>
            <a class="link-grey">
                <div class="sidebar-links-inner"><i class="fa fa-wrench"></i><span class="nav-text">Issues</span></div>
            </a>
        <?php else :?>
            <a class="link-blue <?=3==$_lb_active?" selected":""?>" href="<?= base_url(). 'Issues/list_all/' . $project["bitbucket_repo_name"] ?>">
                <div class="sidebar-links-inner"><i class="fa fa-wrench"></i><span class="nav-text">Issues</span></div>
            </a>
        <?php endif;?>
        <a class="link-blue <?=4==$_lb_active?" selected":""?>" href="<?=base_url().'Usecases/list_all/'.$project["project_id"]?>">
            <div class="sidebar-links-inner"><i class="fa fa-list"></i><span class="nav-text">Use Cases</span></div>
        </a>
        <a class="link-blue <?=5==$_lb_active?" selected":""?>" href="<?=base_url().'Projects/view_report/'.$project["project_id"]?>">
            <div class="sidebar-links-inner"><i class="fa fa-bar-chart"></i><span class="nav-text">Analytics</span></div>
        </a>
        <a class="link-blue <?=6==$_lb_active?" selected":""?>" href="<?=base_url().'upload/upload/'.$project['project_id']?>">
            <div class="sidebar-links-inner"><i class="fa fa-folder"></i><span class="nav-text">File Repository</span></div>
        </a>
    </div>
</aside>