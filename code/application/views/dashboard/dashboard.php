<?php
defined('BASEPATH') OR exit('No direct script access allowed');?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <?php $this->load->view('common/common_header');?>
        <style>
            .panel-heading-eh{
                font-weight: 700;
                text-align: center;
                text-transform: uppercase;
            }
            .panel-body-eh{
                overflow-y: auto;
                height: 150px;
            }
        </style>
        <link href="<?=base_url().'css/material.css'?>" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="<?=base_url()."js/chart_loader.js"?>"></script>
        <script>
            //google.charts.load('current', {'packages': ['bar','corechart']});
            google.charts.load('current', {'packages':['bar','corechart']});
            google.charts.setOnLoadCallback(drawChart);

            function drawChart() {
                var jsonData3 = $.ajax({
                    url: "<?=base_url().'dashboard/num_of_tasks_issue_onging_projects'?>",
                    //url: "http://localhost/tspms/code/dashboard/get_per_issue_data/1",
                    dataType: "json",
                    async: false
                }).responseText;

                var data3 = new google.visualization.DataTable(jsonData3);
                var jsonData5 = $.ajax({
                    url: "<?=base_url().'dashboard/phase_percentage'?>",
                    //url: "http://localhost/tspms/code/dashboard/get_per_issue_data/1",
                    dataType: "json",
                    async: false
                }).responseText;
                var data5 = new google.visualization.DataTable(jsonData5);
                var options3 = {
                    title: 'Task/Issue Analysis',
                    'legend':'bottom',
                    //legend:'bottom',
                    chartArea: { width: '90%',left: "5%" , height: '70%'},
                    vAxis: {
                        title: 'Number of Task/Issue'
                    },
                    hAxis: {
                        title: 'Project Code'
                    },
                    seriesType: 'bars',
                    //colors: ['#1b9e77', '#d95f02'],
                    bars: 'vertical'

                };
                var options5 = {
                    'legend':'right',
                    chartArea: { width: '100%',left: "15%",height: "80%" },
                };
                //console.log(new google.charts.Bar(document.getElementById('chart_div3')));
                var chart3 = new google.charts.Bar(document.getElementById('chart_div3'));
                chart3.draw(data3, options3);
                var pieChart = new google.visualization.PieChart(document.getElementById('pie-chart'));
                pieChart.draw(data5, options5);
            }
        </script>
        <style>
            .badge{
                white-space:nowrap;
            }
        </style>
    </head>
    <body>
<?php
$dashboard = [
    'dashboard_class'=>'active','projects_class'=>'','message_class'=>'', 'customers_class'=>'', 'internal_user_class'=>'', 'analytics_class'=>''
];
$this->load->view('common/pm_nav', $dashboard);
?>
<div class="col-md-offset-1 col-md-10" style="margin-top:20px">
    <div class="col-md-8">
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-danger">
                    <div class="panel-heading-eh panel-heading">Important and Urgent</div>
                    <div class="panel-body panel-body-eh">
                        <table class="table table-condensed ">
                            <?php if(isset($tasks_ui)):
                                foreach($tasks_ui as $t):?>
                                    <tr>
                                        <td><a href="<?=base_url().'projects/view_dashboard/'.$t['project_id']?>"><?=$t['content']?></a></td>
                                        <td> <?php
                                            $day = " days";
                                            if($t['days']==0 || $t['days']==1 || $t['days']==-1){
                                                $day = " day";
                                            }
                                            if($t['days']<0){
                                                $t['days'] = 0-$t['days'];
                                                echo '<span class="badge" style="background-color: indianred">- '.$t['days'].$day.'</span>';

                                            }else{
                                                echo '<span class="badge" style="background-color: darkorange">'.$t['days'].$day.'</span>';
                                            }?></td>
                                    </tr>
                                <?php endforeach; endif?>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-warning">
                    <div class="panel-heading-eh panel-heading">Urgent but not Important</div>
                    <div class="panel-body panel-body-eh">
                        <table class="table table-condensed ">
                            <?php if(isset($tasks_u)):
                                foreach($tasks_u as $t):?>
                                    <tr>
                                        <td><a href="<?=base_url().'projects/view_dashboard/'.$t['project_id']?>"><?=$t['content']?></a></td>
                                        <td> <?php
                                            $day = " days";
                                            if($t['days']==0 || $t['days']==1 || $t['days']==-1){
                                                $day = " day";
                                            }
                                            if($t['days']<0){
                                                $t['days'] = 0-$t['days'];
                                                echo '<span class="badge" style="background-color: indianred">- '.$t['days'].$day.'</span>';
                                            }else{
                                                echo '<span class="badge" style="background-color: darkorange">'.$t['days'].$day.'</span>';
                                            }?></td>
                                    </tr>
                                <?php endforeach; endif?>
                        </table>

                    </div>
                </div>
            </div>

        </div>
        <div class="row">

            <div class="col-md-6 ">
                <div class="panel panel-info">
                    <div class="panel-heading-eh panel-heading">Important but not urgent</div>
                    <div class="panel-body panel-body-eh">
                        <table class="table table-condensed " >
                            <?php if(isset($tasks_i)):
                                foreach($tasks_i as $t):?>
                                    <tr>
                                        <td><a href="<?=base_url().'projects/view_dashboard/'.$t['project_id']?>"><?=$t['content']?></a></td>
                                        <td> <?php
                                            $day = " days";
                                            if($t['days']==0 || $t['days']==1 || $t['days']==-1){
                                                $day = " day";
                                            }
                                            if($t['days']<0){
                                                $t['days'] = 0-$t['days'];
                                                echo '<span class="badge" style="background-color: indianred">- '.$t['days'].$day.'</span>';

                                            }else{
                                                echo '<span class="badge" style="background-color: green">'.$t['days'].$day.'</span>';
                                            }?></td>
                                    </tr>
                                <?php endforeach; endif?>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-6 ">
                <div class="panel panel-success">
                    <div class="panel-heading-eh panel-heading">Not Important or Urgent</div>
                    <div class="panel-body panel-body-eh">
                        <table class="table table-condensed " >
                            <?php if(isset($tasks_none)):
                                foreach($tasks_none as $t):?>
                                    <tr>
                                        <td><a href="<?=base_url().'projects/view_dashboard/'.$t['project_id']?>"><?=$t['content']?></a></td>
                                        <td> <?php
                                            $day = " days";
                                            if($t['days']==0 || $t['days']==1 || $t['days']==-1){
                                                $day = " day";
                                            }
                                            if($t['days']<0){
                                                $t['days'] = 0-$t['days'];
                                                echo '<span class="badge" style="background-color: indianred">- '.$t['days'].$day.'</span>';

                                            }else{
                                                echo '<span class="badge" style="background-color: green">'.$t['days'].$day.'</span>';
                                            }?></td>
                                    </tr>
                                <?php endforeach; endif?>
                        </table>

                    </div>
                </div>
            </div>

        </div>

        <br/>
        <!--div class="col-md-1" id="chart_div5" style="width: 600px; height: 200px;"></div-->
    </div>

    <div class="col-md-4">
        <div class="panel panel-default" style="width: 200px;height: 150px;">
            <div class="panel-heading" style="background: #e0e2e5"><Strong>Total Urgency Score</Strong></div>
            <div class="panel-body" style="height: 200px;">
                <div class="thumbnail calendar-date" >
                    <script>
                        var total_urgency = $.ajax({
                            url: "<?=base_url().'scheduled_tasks/get_issue_urgency_score_across_projects'?>",
                            //url: "http://localhost/tspms/code/dashboard/get_per_issue_data/1",
                            dataType: "float",
                            async: false
                        }).responseText;
                        //window.alert(urgency);
                        document.write(total_urgency);
                    </script>
                </div>
                Urgency Level: <span id = "bage" class="badge" ></span>
            </div>

            <script>
                console.log(total_urgency);
                if(total_urgency ><?=$threshold[0]['High']?> ){
                    document.getElementById("bage").style.backgroundColor = "rgba(200,50,50, 0.7)";
                    $('#bage').append("High");
                }else if(total_urgency ><?=$threshold[0]['Low']?> && total_urgency<= <?=$threshold[0]['High']?>){
                    document.getElementById("bage").style.backgroundColor = "rgba(250,120,0,0.7)";
                    $('#bage').append("Medium");
                }else{
                    document.getElementById("bage").style.backgroundColor = "rgba(44,74,215,0.7)";
                    $('#bage').append("Low");
                }

            </script>
        </div>
        <div>
            <div style="text-align: center">Project Phases</div>
            <div id="pie-chart" style="width: 350px; height: 200px">

            </div>
        </div>
    </div>
    <div class="col-md-12">
    <h3>Task/Issue Analysis</h3>
    <div  id="chart_div3" style="height: 300px;">

    </div>
    </div>

</div>
    </body>
</html>