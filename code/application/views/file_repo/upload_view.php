<?php
defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>File Repository</title>
    <?php $this->load->view('common/common_header');?>
    <link rel="stylesheet" href="<?=base_url().'css/sidebar-left.css'?>">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jstree/3.0.9/themes/default/style.min.css" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/jstree/3.0.9/jstree.min.js"></script>

    <script>
        function init(){
            reset_upload();
            initialize_tree();
        }

        function upload_file(){
            $.ajax({
                url: '<?=base_url().'upload/file_upload/'.$project['project_id']?>',
                type: 'POST',
                data: new FormData($('#upload_file_form')[0]),
                cache: false,
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function(data)
                {
                    console.log(data);
                    if(data.status==="success"){
                        reset_upload();
                        refreshTree();
                    }else{
                        display_upload_form_error(data.message)
                    }
                },
                error: function(jqXHR, textStatus)
                {
                    display_upload_form_error('Ajax Error:'+textStatus);
                    console.log('ERRORS: ' + textStatus);
                }
            });
            disable_upload_form();
        }

        function reset_upload(){
            $("#upload_file_modal").modal('hide');
            $("#upload_message_alert").empty();
            $("#upload_message_alert").hide();
            $("#upload_progress_bar").hide();
            $("#upload_button").show();
            $("#upload_cancel_button").show();
        }

        function disable_upload_form(){
            $("#upload_button").hide();
            $("#upload_progress_bar").show();
            $("#upload_cancel_button").hide();
        }

        function display_upload_form_error(errorMessage){
            $("#upload_message_alert").html(errorMessage);
            $("#upload_message_alert").show();
            $("#upload_progress_bar").hide();
            $("#upload_button").show();
            $("#upload_cancel_button").show();
        }

        function disable_file_delete_modal(){
            $("#fileDeleteModal").modal('hide');
        }

        function refreshTree(){
            $.post( "<?=site_url().'upload/get_all_files/'.$project['project_id']?>", function( data ) {
                $('#tree').jstree(true).settings.core.data = data;
                $('#tree').jstree(true).refresh();
            });
        }

        function initialize_tree() {
            $.post( "<?=site_url().'upload/get_all_files/'.$project['project_id']?>", function( data ) {
                $(function () {
                    $('#tree').jstree({
                        'core': {
                            'check_callback': function(operation, node, node_parent) {
                                if (operation === "move_node") {
                                    return node_parent.original.type === "folder"; //only allow dropping inside nodes of type 'Parent'
                                }
                                return true;
                            },
                            'data': data
                        },
                        "types" : {
                            "default" : {
                                "icon" : "fa fa-file-o"
                            },
                            "image" : {
                                "icon" : "fa fa-picture-o"
                            },
                            "pdf" : {
                                "icon" : "fa fa-file-pdf-o"
                            },
                            "word" : {
                                "icon" : "fa fa-file-word-o"
                            },
                            "ppt" : {
                                "icon" : "fa fa-file-powerpoint-o"
                            },
                            "archive" : {
                                "icon" : "fa fa-file-archive-o"
                            },
                            "excel" : {
                                "icon" : "fa fa-file-excel-o"
                            },
                            "video" : {
                                "icon" : "fa fa-file-video-o"
                            },
                            "audio" : {
                                "icon" : "fa fa-file-audio-o"
                            },
                            "text" : {
                                "icon" : "fa fa-file-text-o"
                            },
                            "code" : {
                                "icon" : "fa fa-file-code-o"
                            },
                            "folder" : {
                                "icon" : "fa fa-folder-open-o"
                            }
                        },
                        contextmenu : {
                            "items" : function () {
                                return {
                                    "view" : {
                                        label: "Open",
                                        action: function() {
                                            open_file();
                                        }
                                    },
                                    "rename" : {
                                        label: "Rename",
                                        action: function() {
                                            rename_file();
                                        }
                                    },
                                    "delete" : {
                                        label: "Delete",
                                        action: function() {
                                            deleteFileButtonClicked();
                                        },
                                        separator_before: true
                                    }
                                }
                            }
                        },
                        "dnd": {
                            check_while_dragging: true
                        },
                        'plugins': [
                            "search", "state", "types", "wholerow", "contextmenu", "dnd"
                        ]
                    }).on("select_node.jstree", function(event, data) {
                        var selectedNode = $('#tree').jstree(true).get_selected('full',true)[0];
                        var fid = selectedNode['original']['id'];

                        $.post( "<?=site_url().'upload/get_by_fid/'?>"+fid, function( data ){
                          if(data['file_type']== 'folder'){
                              $('#info').hide()
                          }else{
                              var full_name=data['file_key'];
                              $('#info').show();
                              $('#format').html('<strong>Type: </strong>'+ full_name.substr( full_name.indexOf('.')));
                              $('#size').html('<strong>Size: </strong>'+ bytesToSize(data['file_size'],0));
                              $('#time').html('<strong>Last modified: </strong>'+data['last_updated']);
                          }

                        });

                    }).on("dblclick", ".jstree-anchor", function(e) {
                        var selectedNode = $('#tree').jstree(true).get_selected('full',true)[0];
                        var link = selectedNode['original']['a_attr']['href'];
                        if(link.substring(0, 4) == "http"){
                            window.open(link);
                        }
                    }).on("rename_node.jstree", function(event, data) {
                        var newName = data.text;
                        var isExistingNode = /^\d+$/.test(data.node.id);

                        if(isExistingNode) {
                            jQuery.ajax({
                                type: "POST",
                                url: '<?=base_url().'upload/rename_file/'.$project['project_id']?>',
                                dataType: 'json',
                                data: {'new_name': newName, 'fid': data.node.id},
                                cache: false,
                                success: function (res) {
                                    console.log(res);
                                    refreshTree();
                                },
                                error: function (jqXHR, textStatus) {
                                    //display_upload_form_error('Ajax Error:'+textStatus);
                                    console.log('ERRORS: ' + textStatus);
                                }
                            });
                        } else {
                            jQuery.ajax({
                                type: 'POST',
                                url: '<?=base_url().'upload/create_folder/'.$project['project_id']?>',
                                dataType: 'json',
                                data: {'folder_name': newName},
                                cache: false,
                                success: function(data)
                                {
                                    console.log(data);
                                    refreshTree();
                                },
                                error: function(jqXHR, textStatus)
                                {
                                    //display_upload_form_error('Ajax Error:'+textStatus);
                                    console.log('ERRORS: ' + textStatus);
                                }
                            });
                        }
                    }).on("move_node.jstree", function(event, data) {
                        jQuery.ajax({
                            type: "POST",
                            url: '<?=base_url().'upload/move_file/'?>',
                            dataType: 'json',
                            data: {'new_parent_id': data.parent, 'fid': data.node.id, 'new_index': data.position},
                            cache: false,
                            success: function (res) {
                                console.log(res);
                            },
                            error: function (jqXHR, textStatus) {
                                //display_upload_form_error('Ajax Error:'+textStatus);
                                console.log('ERRORS: ' + textStatus);
                            }
                        });
                    })
                });

            });
        }

        var selectedNode = null;

        function deleteFileButtonClicked() {
            selectedNode = $('#tree').jstree(true).get_selected('full',true)[0];
            $('#fileDeleteModal').modal('show');
        };

        function confirmFileDeletion() {
            $.post( "<?=site_url().'upload/delete_by_fid/'?>"+selectedNode.id, function( data ) {
                if(data.status=='error'){
                    alert(data.message);
                }
                $('#tree').jstree(true).delete_node(selectedNode);
                selectedNode = null;
            });
            $('#info').hide();
            disable_file_delete_modal()
        }

        function new_folder(){
            var ref = $('#tree').jstree(true);
            var node = ref.create_node('#0', {"text": "New Folder", "type": "folder"});
            ref.edit(node);
        }

        function open_file() {
            selectedNode = $('#tree').jstree(true).get_selected('full',true)[0];
            var link = selectedNode['original']['a_attr']['href'];

            if(link != '#'){
                window.open(link);
            }
        }

        function rename_file(){
            var ref = $('#tree').jstree(true),
                sel = ref.get_selected();
            if(!sel.length) { return false; }
            sel = sel[0];
            ref.edit(sel);
        };

        function bytesToSize(bytes, precision) {
            var kilobyte = 1024;
            var megabyte = kilobyte * 1024;
            var gigabyte = megabyte * 1024;
            var terabyte = gigabyte * 1024;

            if ((bytes >= 0) && (bytes < kilobyte)) {
                return bytes + ' B';

            } else if ((bytes >= kilobyte) && (bytes < megabyte)) {
                return (bytes / kilobyte).toFixed(precision) + ' KB';

            } else if ((bytes >= megabyte) && (bytes < gigabyte)) {
                return (bytes / megabyte).toFixed(precision) + ' MB';

            } else if ((bytes >= gigabyte) && (bytes < terabyte)) {
                return (bytes / gigabyte).toFixed(precision) + ' GB';

            } else if (bytes >= terabyte) {
                return (bytes / terabyte).toFixed(precision) + ' TB';

            } else {
                return bytes + ' B';
            }
        }
    </script>
</head>

<body onload="init()">
<?php
$class = [
    'dashboard_class'=>'',
    'projects_class'=>'active',
    'message_class'=>'',
    'customers_class'=>'',
    'internal_user_class'=>'',
    'analytics_class'=>''
];
$this->load->view('common/pm_nav', $class);
//load sidebar
$this->load->view('common/side_bar', ["_lb_active"=>6,"project"=>$project]);
?>



<div class="col-md-offset-1 col-md-10 content">
    <!-- Page Content -->
    <div class="col-md-12">
        <h1 class="page-header"> File Repository - <?=$project['project_title']?></h1>
    </div>

    <div class="modal fade" id="upload_file_modal" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Upload File</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-info alert-dismissible" role="alert" id="upload_message_alert">
                    </div>
                    <form name="upload_file_form" id="upload_file_form" method="post" enctype="multipart/form-data" action="<?=base_url().'upload/file_upload'?>">
                        <div class="form-group">
                            <label for="file_input">Select file</label>
                            <input type="hidden" name="MAX_FILE_SIZE" value="10485760">
                            <input type="file" id="file_to_upload" name="file_to_upload">
                        </div>
                    </form>
                </div>

                <div class="modal-footer">
                    <div class="progress" name="upload_progress_bar" id="upload_progress_bar">
                        <div class="progress-bar progress-bar-striped active" role="progressbar" style="width:100%" >
                            Uploading... Please wait
                        </div>
                    </div>
                    <button type="button" class="btn btn-default pull-left" onclick="reset_upload()" id="upload_cancel_button">Cancel</button>
                    <button type="button" class="btn btn-primary" onclick="upload_file()" id="upload_button">Upload</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal fade" id="fileDeleteModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <strong>Delete File</strong>
                </div>
                <div class="modal-body">
                    This action cannot be undone, do you wish to proceed?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-danger" id="btnYes" onclick="confirmFileDeletion()"> Delete </button>
                </div>
            </div>
        </div>
    </div>

    <form id="search">
        <div class="col-md-3">
            <input class="form-control" type="search" id="query"/>
        </div>
        <div class="col-md-9">
            <button class="btn btn-default" type="submit"><i class="fa fa-search"></i>&nbsp;Search</button>
        </div>

    </form>

    <div class="col-md-8" style="margin-top:10px;">
        <div style ="margin-left=5px">
            <button class="btn btn-sm btn-primary" onclick="$('#upload_file_modal').modal('show')"><i class="fa fa-plus"></i>&nbsp;Upload</button>
            <button type="button" class="btn btn-sm btn-primary"  onclick="new_folder();"><i class="fa fa-folder"></i>&nbsp;New Folder</button>
            <button type="button" class="btn btn-success btn-sm"  onclick="open_file();"><i class="fa fa-file"></i>&nbsp;Open</button>
            <button type="button" class="btn btn-info btn-sm"  onclick="rename_file();"><i class="fa fa-pencil"></i>&nbsp;Rename</button>
            <button type="button" class="btn btn-warning btn-sm" onclick="deleteFileButtonClicked();"><i class="fa fa-trash"></i>&nbsp;Delete</button>
        </div>
        <div id="tree" style="background-color: #f5f5f5"></div>
    </div>
    <div class="col-md-4" id="info" style="display:none">
        <br><br>
        <div class="well">
            <h4>File Details</h4>
            <ul class="list-group">
                <li class="list-group-item" id="format"></li>
                <li class="list-group-item" id="time"><strong>Last Modified: </strong></li>
                <li class="list-group-item" id="size"><strong>Size:</strong></li>
            </ul>
        </div>
        </div>


    <script>
        $("#search").submit(function(e) {
            e.preventDefault();
            $("#tree").jstree(true).search($("#query").val());
        });
    </script>
</div>

</body>

</html>