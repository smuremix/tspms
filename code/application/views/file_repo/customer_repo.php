<?php
defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>File Repository</title>
    <?php $this->load->view('common/common_header');?>
    <link rel="stylesheet" href="<?=base_url().'css/sidebar-left.css'?>">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jstree/3.0.9/themes/default/style.min.css" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/jstree/3.0.9/jstree.min.js"></script>

    <script>
        function init(){
            initialize_tree();
        }

        function initialize_tree() {
            $.post( "<?=site_url().'upload/get_all_files/'.$project['project_id']?>", function( data ) {
                $(function () {
                    $('#tree').jstree({
                        'core': {
                            'data': data
                        },
                        "types" : {
                            "default" : {
                                "icon" : "fa fa-file-o"
                            },
                            "image" : {
                                "icon" : "fa fa-picture-o"
                            },
                            "pdf" : {
                                "icon" : "fa fa-file-pdf-o"
                            },
                            "word" : {
                                "icon" : "fa fa-file-word-o"
                            },
                            "ppt" : {
                                "icon" : "fa fa-file-powerpoint-o"
                            },
                            "archive" : {
                                "icon" : "fa fa-file-archive-o"
                            },
                            "excel" : {
                                "icon" : "fa fa-file-excel-o"
                            },
                            "video" : {
                                "icon" : "fa fa-file-video-o"
                            },
                            "audio" : {
                                "icon" : "fa fa-file-audio-o"
                            },
                            "text" : {
                                "icon" : "fa fa-file-text-o"
                            },
                            "code" : {
                                "icon" : "fa fa-file-code-o"
                            },
                            "folder" : {
                                "icon" : "fa fa-folder-open-o"
                            }
                        },
                        contextmenu : {
                            "items" : function () {
                                return {
                                    "view" : {
                                        label: "Open",
                                        action: function() {
                                            open_file();
                                        }
                                    }
                                }
                            }
                        },
                        'plugins': [
                            "search", "state", "types", "wholerow", "contextmenu"
                        ]
                    }).on("select_node.jstree", function(event, data) {
                        var selectedNode = $('#tree').jstree(true).get_selected('full',true)[0];
                        var fid = selectedNode['original']['id'];

                        $.post( "<?=site_url().'upload/get_by_fid/'?>"+fid, function( data ) {
                            if(data['file_type']== 'folder'){
                                $('#info').hide()
                            }else{
                                var full_name=data['file_key'];
                                $('#info').show();
                                $('#format').html('<strong>Type: </strong>'+ full_name.substr( full_name.indexOf('.')));
                                $('#size').html('<strong>Size: </strong>'+ bytesToSize(data['file_size'],0));
                                $('#time').html('<strong>Last modified: </strong>'+data['last_updated']);
                            }
                        });
                    }).on("dblclick", ".jstree-anchor", function(e) {
                        var selectedNode = $('#tree').jstree(true).get_selected('full',true)[0];
                        var link = selectedNode['original']['a_attr']['href'];
                        if(link.substring(0, 4) == "http"){
                            window.open(link);
                        }
                    })
                });
            });
        }

        var selectedNode = null;

        function open_file() {
            selectedNode = $('#tree').jstree(true).get_selected('full',true)[0];
            var link = selectedNode['original']['a_attr']['href'];

            if(link != '#'){
                window.open(link);
            }
        }
    </script>
</head>

<body onload="init()">
<?php
$class = [
    'dashboard_class'=>'',
    'projects_class'=>'active',
    'message_class'=>'',
    'customers_class'=>'',
    'internal_user_class'=>'',
    'analytics_class'=>''
];
$this->load->view('common/customer_nav', $class);
$this->load->view('common/side_bar_cust', ["_lb_active"=>3,"project_id"=>$project['project_id']]);
?>


<div class="col-md-offset-1 col-md-10 content">
    <!-- Page Content -->
    <div class="col-md-12">
        <h1 class="page-header"> File Repository - <?=$project['project_title']?></h1>
    </div>

    <form id="search">
        <div class="col-md-3">
            <input class="form-control" type="search" id="query"/>
        </div>
        <div class="col-md-9">
            <button class="btn btn-default" type="submit"><i class="fa fa-search"></i>&nbsp;Search</button>
        </div>
    </form>

    <div class="col-md-8" style="margin-top:10px;">
        <div style ="margin-left=5px">
            <button type="button" class="btn btn-success btn-sm"  onclick="open_file();"><i class="fa fa-file"></i>&nbsp;Open</button>
        </div>
        <div id="tree" style="background-color: #f5f5f5"></div>
    </div>

    <div class="col-md-4" id="info" style="display:none">
        <br><br>
        <div class="well">
            <h4>File Details</h4>
            <ul class="list-group">
                <li class="list-group-item" id="format"></li>
                <li class="list-group-item" id="time"><strong>Last Modified: </strong></li>
                <li class="list-group-item" id="size"><strong>Size:</strong></li>
            </ul>
        </div>
    </div>

    <script>
        $("#search").submit(function(e) {
            e.preventDefault();
            $("#tree").jstree(true).search($("#query").val());
        });
    </script>
</div>

</body>

</html>