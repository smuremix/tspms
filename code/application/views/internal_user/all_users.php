
<?php
defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('common/common_header');?>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css">
    <script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>

    <script>
        $(document).ready(function(){
            $('#userTable').dataTable();

        });
    </script>
</head>
<body>
<?php
$class = [
    'dashboard_class'=>'',
    'projects_class'=>'',
    'message_class'=>'',
    'customers_class'=>'',
    'internal_user_class'=>'active',
    'analytics_class'=>''
];
$this->load->view('common/pm_nav', $class);
?>

<div class="col-md-offset-1 col-md-10">
    <!-- Page Content -->
    <div class="col-md-12">
        <h1 class="page-header">
            Internal Users&nbsp;
            <a href="<?=base_url('internal_users/add')?>" class=" btn btn-primary"><i class="fa fa-plus"></i>&nbsp; New User</a>
        </h1>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?php if($this->session->userdata('message')):?>
                <div class="form-group">
                    <div class="alert alert-info " role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                        <?=$this->session->userdata('message')?>
                    </div>
                </div>
                <?php $this->session->unset_userdata('message') ?>
            <?php endif;?>
            <table class="table table-striped" id="userTable">
                <thead>
                <th>Username</th>
                <th>Name</th>
                <th>Email</th>
                <th>BB username</th>
                <th>Type</th>
                <th>Status</th>
                <th></th>
                </thead>

                <?php if(!false == $users):?>
                    <?php foreach($users as $user):?>
                        <tr>
                            <td><?=$user['username']?></td>
                            <td><?=$user['name']?></td>
                            <td><?=$user['email']?></td>
                            <td><?=$user['bb_username']?></td>
                            <td><?=$user['type']?></td>
                            <td>
                                <?php if($user['is_active']==1):?>
                                    Active
                                <?php else:?>
                                    Deactivated
                                <?php endif?>
                            </td>
                            <td><a href="<?=base_url().'internal_users/edit/'.$user["u_id"]?>" class="btn btn-primary" type="button" ><i class="fa fa-pencil-square-o"></i></a></td>
                        </tr>
                    <?php endforeach?>
                <?php endif?>
            </table>
        </div>
    </div>
</div>

<div class="col-md-offset-1 col-md-10">
    <!-- Page Content -->
    <div class="col-md-12">
        <h1 class="page-header">
            Environment Setting&nbsp;
            <!-- Pending -->
            <button class="btn btn-primary" title="edit env_factor.." data-toggle="modal" data-target="#editEnvFactor"><i class="fa fa-pencil"></i>&nbsp;Edit</button>
        </h1>
    </div>

    <div class="modal fade" id="editEnvFactor" tabindex="-1" role="dialog" >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" >Edit Environment Factor</h4>
                </div>

                <form id="newTask" data-parsley-validate role="form" action="<?=base_url().'Internal_users/edit_env_factor'?>" method="post">

                    <!--<div class="modal-body">-->
                    <div>
                        <?php if(!false == $users):?>
                            <?php foreach($environment as $env):?>
                                <div class="form-group col-md-6">
                                    <label for="distributed_system"><?=$env['Description']?></label>
                                    <?php
                                        if($env['type']=="UCP"){
                                            ?>
                                            <select type="number" name="<?=$env['Description']?>" id="<?=$env['Description']?>" class="form-control" data-parsley-required min="1" max="5">
                                                <?php for($v=1;$v<=5;$v++):?>
                                                    <option value="<?=$v?>"><?=$v?></option>
                                                <?php endfor;?>
                                            </select>

                                            <?php
                                        }else{
                                            ?>

                                            <select type="number" name="<?=$env['Description']?>" id="<?=$env['Description']?>" class="form-control" data-parsley-required min="1" max="10">
                                                <?php for($v=1;$v<=10;$v++):?>
                                                    <option value="<?=$v?>"><?=$v?></option>
                                                <?php endfor;?>
                                            </select>
                                            <?php
                                        }
                                    ?>

                                </div>
                            <?php endforeach?>
                        <?php endif?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <input type="submit" name="submit" id="submit" class="btn btn-primary" value="Submit">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?php if($this->session->userdata('message')):?>
                <div class="form-group">
                    <div class="alert alert-info " role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                        <?=$this->session->userdata('message')?>
                    </div>
                </div>
                <?php $this->session->unset_userdata('message') ?>
            <?php endif;?>
            <table class="table table-striped" id="userTable">
                <thead>
                <th>Factor</th>
                <th>Description</th>
                <th>Weight</th>
                <th>Assigned Value</th>
                <th>Type</th>
                </thead>

                <?php if(!false == $users):?>
                    <?php foreach($environment as $env):?>
                        <tr>
                            <td><?=$env['Factor_ID']?></td>
                            <td><?=$env['Description']?></td>
                            <td><?=$env['Weight']?></td>
                            <td><?=$env['assigned_value']?></td>
                            <td><?=$env['type']?></td>
                        </tr>
                    <?php endforeach?>
                <?php endif?>
            </table>
        </div>
    </div>
</div>

</body>
</html>