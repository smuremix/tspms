<?php
defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('common/common_header');?>
    <link rel="stylesheet" href="<?=base_url().'css/sidebar-left.css'?>">
    <script src="<?= base_url() . 'js/plugins/ckeditor/ckeditor.js' ?>"></script>
</head>
<body >
<?php
$class = [
    'dashboard_class'=>'',
    'projects_class'=>'active',
    'message_class'=>'',
    'customers_class'=>'',
    'internal_user_class'=>'',
    'analytics_class'=>''
];
$this->load->view('common/pm_nav', $class);

$this->load->view('common/side_bar', ["_lb_active"=>4,"project"=>$project]);
?>

<div class="container content">
        <h1 class="page-header">
            Edit Use Case&nbsp;
            <a href="<?= base_url() . 'Usecases/list_all/'.$project['project_id'] ?>" class="btn btn-primary"><i class="fa fa-backward"></i>&nbsp;Back</a>
        </h1>
        <form  data-parsley-validate role="form" action="<?=base_url().'Usecases/edit_use_case/'.$usecase['usecase_id']?>" method="post">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="title">Title*:</label>
                    <input name="title" id="title"  type="text" class="form-control" value="<?=$usecase['title']?>" data-parsley-required>
                </div>
                <div class="form-group">
                    <label for="stakeholders">Stakeholders:</label>
                    <textarea class="form-control" name="stakeholders" id="stakeholders" rows="2" ><?=$usecase['stakeholders']?></textarea>
                </div>
                <div class="form-group">
                    <label for="flow">Flow:</label>
                    <textarea name="flow" id="flow" rows="3" ><?=$usecase['flow']?></textarea>
                    <script>
                        CKEDITOR.replace( 'flow' );
                    </script>
                </div>

            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="classification">Classification:</label>
                    <select class="form-control" id="classification" name="classification" data-parsley-required>
                        <option value="Simple" <?=set_select("classification","Simple",$usecase['classification']=="Simple")?>>Simple</option>
                        <option value="Average" <?=set_select("classification","Average",$usecase['classification']=="Average")?>>Average</option>
                        <option value="Complex"<?=set_select("classification","Complex",$usecase['classification']=="Complex")?>>Complex</option>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="type">Type:</label>
                    <select class="form-control" id="type" name="type" data-parsley-required>
                        <option value="Internal"<?=set_select("type","Internal",$usecase['type']=="Internal")?>>Internal</option>
                        <option value="External" <?=set_select("type","External",$usecase['type']=="External")?>>External</option>
                    </select>
                </div>
            </div>

            <div class="col-md-12 pull-right">
                <input type="submit" name="submit" id="submit" class="btn btn-primary" value="Submit">
                <!--<a href="//?base_url().'Customers/edit/'.$c["c_id"]?" class="btn btn-primary">Submit</a>-->
                <a href="<?= base_url() . 'Usecases/list_all/'.$project['project_id'] ?>" class="btn btn-default">Cancel</a>
            </div>


        </form>

    </div>



</body>
</html>