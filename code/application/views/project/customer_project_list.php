<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('common/common_header');?>
    <link href="<?=base_url().'css/timeline.css'?>" rel="stylesheet">
    <link rel="stylesheet" href="<?=base_url().'css/sidebar-left.css'?>">
    <style>
        td {
            border-top: none !important;
        }
    </style>
</head>
<body>
<?php
    $class = ['projects_class'=>'active','message_class'=>''];
    $this->load->view('common/customer_nav', $class);
?>
<div class="col-md-offset-1 col-md-10">
    <h1 class="page-header" style="padding-top:30px">My Projects</h1>
    <!-- Page Content -->

    <!-- /.row -->
    <div class="row">
       <?php foreach($projects as $p):?>
        <div class="col-lg-3 col-md-4 col-sm-6" style="min-width: 200px;max-width:300px">
            <div class="panel project-panel <?=$p['is_ongoing']==1? " ongoing-panel": " past-panel"?>" link="<?=base_url("projects/customer_view/".$p['project_id'])?>">
                <div class="panel-heading project-panel-heading" style="text-align:center;height: 70px;" ><strong><?=$p['project_title']?></strong></div>
                <div class="panel-body" style="font-size:15px ">
                    <table class="table table-condensed">
                        <tr>
                            <td><i class="fa fa-calendar-check-o"></i>&nbsp;<strong>Current Stage </strong></td>
                            <td><?= $p['phase_name']? $p['phase_name']:"not started"?></td>
                        </tr>
                        <tr>
                            <td> <i class="fa fa-link"></i>&nbsp;<strong>Preview </strong></td>
                            <?php if($p['customer_preview_link']):?>
                                <td> <a href="<?=$p['customer_preview_link']?>" class="clickable" target="_blank">Click here</a></td>
                            <?php else:?> <td><i>N.A.</i></td> <?php endif;?>
                        </tr>
                    </table>
<!--                    <a href="" class="btn pull-right btn-info"><i class="fa fa-eye"></i> &nbsp;View</a>-->

                </div>
            </div>
        </div>
        <?php endforeach?>
    </div>
    <script>
        $(".project-panel").on("click",function(event){
            if(event.which==1 && !$(event.target).hasClass("clickable") && !$(event.target).is( "a" )){
                window.location.href = $(this).attr("link");
            }
        });
        $(".clickable").click(function(e) {e.stopPropagation();});
    </script>
</div>
</body>
</html>