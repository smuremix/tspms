
<?php
defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('common/common_header');?>
    <link rel="stylesheet" href="<?=base_url().'css/sidebar-left.css'?>">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css">
    <script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function(){
            $('#usecaseTable').dataTable();
        });

        function deleteButtonClicked(uc_id) {
            $('#deleteModal').data('uc_id', uc_id).modal('show');
        }

        function deleteButtonClicked_actor(actor_id) {
            $('#deleteModal_actor').data('actor_id', actor_id).modal('show');
        }

        function confirmDelete() {
            // handle deletion here
            var uc_id = $('#deleteModal').data('uc_id');
            //to be change to delete milestone controller
            var delete_url = "<?= base_url().'Usecases/delete_usecase/'?>" + uc_id;
            window.location.href = delete_url;
        }

        function confirmDelete_actor() {
            // handle deletion here
            var actor_id = $('#deleteModal_actor').data('actor_id');
            //to be change to delete milestone controller
            var delete_url = "<?= base_url().'Usecases/delete_actor/'?>" + actor_id;
            window.location.href = delete_url;
        }
    </script>
</head>
<body>
<?php
$class = [
    'dashboard_class'=>'',
    'projects_class'=>'active',
    'message_class'=>'',
    'customers_class'=>'',
    'internal_user_class'=>'',
    'analytics_class'=>''
];
$is_dev = $this->session->userdata('internal_type')=='Developer';
if($this->session->userdata('internal_type')=='Developer') {
    $this->load->view('common/dev_nav', $class);
    ?>
    <aside class="sidebar-left">
        <div class="sidebar-links">
            <a class="link-blue " href="<?=base_url()?>Issues/list_all/<?=$repo_slug?>">
                <div class="sidebar-links-inner"><i class="fa fa-wrench"></i><span class="nav-text">Issues</span></div>
            </a>
            <a class="link-blue selected" href="<?=base_url().'Usecases/list_all/'.$project["project_id"]?>">
                <div class="sidebar-links-inner"><i class="fa fa-list"></i><span class="nav-text">Use Case List</span></div>
            </a>
        </div>

    </aside>
    <?php
}else {
    $this->load->view('common/pm_nav', $class);
    //load sidebar
    $this->load->view('common/side_bar', ["_lb_active"=>4,"project"=>$project]);
} ?>


<div class="container content">
    <h1 class="page-header">
        Use Case List&nbsp;
        <?php if(!$is_dev):?>
        <a href="<?= base_url() . 'Usecases/new_use_case/'.$project["project_id"] ?>"  class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp;Add</a>&nbsp;
        <?php endif;?>
    </h1>
    <?php if($this->session->userdata('message')):?>
        <div class="form-group">
            <div class="alert alert-info " role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <?=$this->session->userdata('message')?>
            </div>
        </div>
        <?php $this->session->unset_userdata('message') ?>
    <?php endif;?>
    <table class="table table-responsive" id="usecaseTable">
        <thead>
        <th>ID</th>
        <th>Title</th>
        <th>Classification</th>
        <th>Type</th>
        <th>Last Updated</th>
        <th></th>
        </thead>
        <?php if(isset($usecases)):?>
            <?php foreach($usecases as $u):?>
                <tr><td><?=$u['sub_id']?></td>
                    <td><?=$u['title']?></td>
                    <td><?=$u['classification']?></td>
                    <td><?=$u['type']?></td>
                    <td><?=$u['last_updated']?></td>
                    <td><a href="<?= base_url() . 'Usecases/edit_use_case/'.$u["usecase_id"] ?>" class="btn btn-default" type="button" ><i class="fa fa-pencil-square-o"></i></a>
                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#detailModal<?=$u['usecase_id']?>" ><i class="fa fa-eye"></i></button>
                        <button class="btn btn-default" type="button" onclick="deleteButtonClicked(<?=$u['usecase_id']?>)" ><i class="fa fa-trash" ></i></button>
                    </td>
                </tr>
            <?php endforeach?>
        <?php endif?>
    </table>
</div>

<div class="container content">
    <h1 class="page-header">
        Actors &nbsp;
        <?php if(!$is_dev):?>
        <a href="<?= base_url() . 'Usecases/new_actor/'.$project["project_id"] ?>"  class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp;Add</a>&nbsp;
        <?php endif;?>
    </h1>
    <?php if($this->session->userdata('message')):?>
        <div class="form-group">
            <div class="alert alert-info " role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <?=$this->session->userdata('message')?>
            </div>
        </div>
        <?php $this->session->unset_userdata('message') ?>
    <?php endif;?>
    <table class="table table-responsive" id="usecaseTable">
        <thead>
        <th>ID</th>
        <th>Actor_Name</th>
        <th>Classification</th>
        <th>Last Updated</th>
        <th></th>
        </thead>
        <?php if(isset($actor)):?>
            <?php foreach($actor as $a):?>
                <tr><td><?=$a['sub_id']?></td>
                    <td><?=$a['actor_name']?></td>
                    <td><?=$a['classification']?></td>
                    <td><?=$a['last_updated']?></td>
                    <td><a href="<?= base_url() . 'Usecases/edit_actor/'.$a["actor_id"] ?>" class="btn btn-default" type="button" ><i class="fa fa-pencil-square-o"></i></a>
                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#detailModal_actor<?=$a['actor_id']?>" ><i class="fa fa-eye"></i></button>
                        <button class="btn btn-default" type="button" onclick="deleteButtonClicked_actor(<?=$a['actor_id']?>)" ><i class="fa fa-trash" ></i></button>
                    </td>
                </tr>
            <?php endforeach?>
        <?php endif?>
    </table>
</div>


<!-- Delete Modal-->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <strong>Delete Use Case</strong>
            </div>
            <div class="modal-body">
                This action cannot be undone, do you wish to proceed?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger" id="btnYes" onclick="confirmDelete()"> Delete </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="deleteModal_actor" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <strong>Delete Actor</strong>
            </div>
            <div class="modal-body">
                This action cannot be undone, do you wish to proceed?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger" id="btnYes" onclick="confirmDelete_actor()"> Delete </button>
            </div>
        </div>
    </div>
</div>

<?php if(isset($usecases)):?>
<?php foreach($usecases as $u):?>
        <div class="modal fade" id="detailModal<?=$u['usecase_id']?>" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4>Use Case Detail</h4>
                    </div>
                    <div class="modal-body">
                        <p><strong>Title: </strong> </p>
                        <?=$u['title']?>
                        <hr>
                        <p><strong>Stakeholders: </strong> </p>
                        <?=$u['stakeholders']?>
                        <hr>
                        <p><strong>Flow: </strong> </p>
                        <?=$u['flow']?>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach?>
<?php endif?>


<?php if(isset($actor)):?>
    <?php foreach($actor as $a):?>
        <div class="modal fade" id="detailModal_actor<?=$a['actor_id']?>" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4>Actor Detail</h4>
                    </div>
                    <div class="modal-body">
                        <p><strong>Actor Name: </strong> </p>
                        <?=$a['actor_name']?>
                        <hr>
                        <p><strong>Classification: </strong> </p>
                        <?=$a['classification']?>
                        <hr>
                        <p><strong>Description: </strong> </p>
                        <?=$a['description']?>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach?>
<?php endif?>


<div class="container content">
    <h1 class="page-header">
        Technical Complexity Factor &nbsp;
        <?php if(!$is_dev):?>
        <button class="btn btn-primary" title="edit tech_factor.." data-toggle="modal" data-target="#editTechFactor"><i class="fa fa-pencil"></i>&nbsp;Edit</button>
        <?php endif?>
    </h1>

    <!--edit tech_factor modal-->
    <div class="modal fade" id="editTechFactor" tabindex="-1" role="dialog" >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" >Edit Tech Factor</h4>
                </div>

                <form id="newTask" data-parsley-validate role="form" action="<?=base_url().'Projects/edit_tech_factor/'.$project['project_id']?>" method="post">
                    <div class="modal-body">
                        <div class="form-group col-md-6">
                            <label for="distributed_system">Distributed system: </label>
                            <select type="number" name="distributed_system" id="distributed_system" class="form-control" data-parsley-required min="1" max="5">
                                <?php for($v=1;$v<=5;$v++):?>
                                    <option value="<?=$v?>"><?=$v?></option>
                                <?php endfor;?>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="end_user_efficiency">End-user efficiency:</label>
                            <select type="number" name="end_user_efficiency" id="end_user_efficiency" class="form-control" data-parsley-required min="1" max="5">
                                <?php for($v=1;$v<=5;$v++):?>
                                    <option value="<?=$v?>"><?=$v?></option>
                                <?php endfor;?>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="internal_processing_complexity">Internal processing complexity:</label>
                            <select type="number" name="internal_processing_complexity" id="internal_processing_complexity" class="form-control" data-parsley-required min="1" max="5">
                                <?php for($v=1;$v<=5;$v++):?>
                                    <option value="<?=$v?>"><?=$v?></option>
                                <?php endfor;?>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="code_reusability">Code reusability:</label>
                            <select type="number" name="code_reusability" id="code_reusability" class="form-control" data-parsley-required min="1" max="5">
                                <?php for($v=1;$v<=5;$v++):?>
                                    <option value="<?=$v?>"><?=$v?></option>
                                <?php endfor;?>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="easy_to_install">Easy to install:</label>
                            <select type="number" name="easy_to_install" id="easy_to_install" class="form-control" data-parsley-required min="1" max="5">
                                <?php for($v=1;$v<=5;$v++):?>
                                    <option value="<?=$v?>"><?=$v?></option>
                                <?php endfor;?>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="easy_to_use">Easy to use:</label>
                            <select type="number" name="easy_to_use" id="easy_to_use" class="form-control" data-parsley-required min="1" max="5">
                                <?php for($v=1;$v<=5;$v++):?>
                                    <option value="<?=$v?>"><?=$v?></option>
                                <?php endfor;?>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="portability_to_other_platforms">Portability to other platforms:</label>
                            <select type="number" name="portability_to_other_platforms" id="portability_to_other_platforms" class="form-control" data-parsley-required min="1" max="5">
                                <?php for($v=1;$v<=5;$v++):?>
                                    <option value="<?=$v?>"><?=$v?></option>
                                <?php endfor;?>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="system_maintenance">System maintenance:</label>
                            <select type="number" name="system_maintenance" id="system_maintenance" class="form-control" data-parsley-required min="1" max="5">
                                <?php for($v=1;$v<=5;$v++):?>
                                    <option value="<?=$v?>"><?=$v?></option>
                                <?php endfor;?>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="concurrent_processings">Concurrent/parallel processings:</label>
                            <select type="number" name="concurrent_processings" id="concurrent_processings" class="form-control" data-parsley-required min="1" max="5">
                                <?php for($v=1;$v<=5;$v++):?>
                                    <option value="<?=$v?>"><?=$v?></option>
                                <?php endfor;?>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="security_features">Security features:</label>
                            <select type="number" name="security_features" id="security_features" class="form-control" data-parsley-required min="1" max="5">
                                <?php for($v=1;$v<=5;$v++):?>
                                    <option value="<?=$v?>"><?=$v?></option>
                                <?php endfor;?>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="access_for_third_parties">Access for third parties:</label>
                            <select type="number" name="access_for_third_parties" id="access_for_third_parties" class="form-control" data-parsley-required min="1" max="5">
                                <?php for($v=1;$v<=5;$v++):?>
                                    <option value="<?=$v?>"><?=$v?></option>
                                <?php endfor;?>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="end_user_training">End user training:</label>
                            <select type="number" name="end_user_training" id="end_user_training" class="form-control" data-parsley-required min="1" max="5">
                                <?php for($v=1;$v<=5;$v++):?>
                                    <option value="<?=$v?>"><?=$v?></option>
                                <?php endfor;?>
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="response_time">Response time/performance objectives:</label>
                            <select type="number" name="response_time" id="response_time" class="form-control" data-parsley-required min="1" max="5">
                                <?php for($v=1;$v<=5;$v++):?>
                                    <option value="<?=$v?>"><?=$v?></option>
                                <?php endfor;?>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <input type="submit" name="submit" id="submit" class="btn btn-primary" value="Submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--End of Tech Factor Modal-->

    <?php if($this->session->userdata('message')):?>
        <div class="form-group">
            <div class="alert alert-info " role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <?=$this->session->userdata('message')?>
            </div>
        </div>
        <?php $this->session->unset_userdata('message') ?>
    <?php endif;?>
    <table class="table table-responsive" id="usecaseTable">
        <thead>
        <th>Factor</th>
        <th>Description</th>
        <th>Weight</th>
        <th>Assigned Value</th>
        <th>Weight x Assigned Value</th>

        </thead>
            <tr><td>F1</td>
                <td>Distributed system</td>
                <td>2.0</td>
                <td><?=$project['distributed_system']?></td>
                <td>
                    <?php
                    if($project['distributed_system']==null){
                        echo "please assign value to the factor";
                    }
                    else{
                        echo $project['distributed_system']*2.0;
                    }
                    ?>
                </td>
            </tr>

        <tr><td>F2</td>
            <td>Response time/performance objectives</td>
            <td>1.0</td>
            <td><?=$project['response_time']?></td>
            <td>
                <?php
                if($project['response_time']==null){
                    echo "please assign value to the factor";
                }
                else{
                    echo $project['response_time'];
                }
                ?>
            </td>
        </tr>

        <tr><td>F3</td>
            <td>End-user efficiency</td>
            <td>1.0</td>
            <td><?=$project['end_user_efficiency']?></td>
            <td>
                <?php
                if($project['end_user_efficiency']==null){
                    echo "please assign value to the factor";
                }
                else{
                    echo $project['end_user_efficiency'];
                }
                ?>
            </td>
        </tr>

        <tr><td>F4</td>
            <td>Internal processing complexity</td>
            <td>1.0</td>
            <td><?=$project['internal_processing_complexity']?></td>
            <td>
                <?php
                if($project['internal_processing_complexity']==null){
                    echo "please assign value to the factor";
                }
                else{
                    echo $project['internal_processing_complexity'];
                }
                ?>
            </td>
        </tr>

        <tr><td>F5</td>
            <td>Code reusability</td>
            <td>1.0</td>
            <td><?=$project['code_reusability']?></td>
            <td>
                <?php
                if($project['code_reusability']==null){
                    echo "please assign value to the factor";
                }
                else{
                    echo $project['code_reusability'];
                }
                ?>
            </td>
        </tr>

        <tr><td>F6</td>
            <td>Easy to install</td>
            <td>0.5</td>
            <td><?=$project['easy_to_install']?></td>
            <td>
                <?php
                if($project['easy_to_install']==null){
                    echo "please assign value to the factor";
                }
                else{
                    echo $project['easy_to_install']*0.5;
                }
                ?>
            </td>
        </tr>

        <tr><td>F7</td>
            <td>Easy to use</td>
            <td>0.5</td>
            <td><?=$project['easy_to_use']?></td>
            <td>
                <?php
                if($project['easy_to_use']==null){
                    echo "please assign value to the factor";
                }
                else{
                    echo $project['easy_to_use']*0.5;
                }
                ?>
            </td>
        </tr>

        <tr><td>F8</td>
            <td>Portability to other platforms</td>
            <td>2.0</td>
            <td><?=$project['portability_to_other_platforms']?></td>
            <td>
                <?php
                if($project['portability_to_other_platforms']==null){
                    echo "please assign value to the factor";
                }
                else{
                    echo $project['portability_to_other_platforms']*2.0;
                }
                ?>
            </td>
        </tr>

        <tr><td>F9</td>
            <td>System maintenance</td>
            <td>1.0</td>
            <td><?=$project['system_maintenance']?></td>
            <td>
                <?php
                if($project['system_maintenance']==null){
                    echo "please assign value to the factor";
                }
                else{
                    echo $project['system_maintenance'];
                }
                ?>
            </td>
        </tr>

        <tr><td>F10</td>
            <td>Concurrent/parallel processing</td>
            <td>1.0</td>
            <td><?=$project['concurrent_processing']?></td>
            <td>
                <?php
                if($project['concurrent_processing']==null){
                    echo "please assign value to the factor";
                }
                else{
                    echo $project['concurrent_processing'];
                }
                ?>
            </td>
        </tr>

        <tr><td>F11</td>
            <td>Security features</td>
            <td>1.0</td>
            <td><?=$project['security_features']?></td>
            <td>
                <?php
                if($project['security_features']==null){
                    echo "please assign value to the factor";
                }
                else{
                    echo $project['security_features'];
                }
                ?>
            </td>
        </tr>

        <tr><td>F12</td>
            <td>Access for third parties</td>
            <td>1.0</td>
            <td><?=$project['access_for_third_parties']?></td>
            <td>
                <?php
                if($project['access_for_third_parties']==null){
                    echo "please assign value to the factor";
                }
                else{
                    echo $project['access_for_third_parties'];
                }
                ?>
            </td>
        </tr>

        <tr><td>F13</td>
            <td>End user training</td>
            <td>1.0</td>
            <td><?=$project['end_user_training']?></td>
            <td>
                <?php
                if($project['end_user_training']==null){
                    echo "please assign value to the factor";
                }
                else{
                    echo $project['end_user_training'];
                }
                ?>
            </td>
        </tr>
    </table>
</div>

</body>
</html>