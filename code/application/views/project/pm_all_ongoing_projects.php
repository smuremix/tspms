
<?php
defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('common/common_header');?>
    <style>
        .glyphicon-refresh{cursor: hand;}
        .glyphicon-refresh-animate {
            -animation: spin .7s infinite linear;
            -webkit-animation: spin2 .7s infinite linear;
        }


        .page-header { border: none; }
        td { border-top: none !important; }
        .update-issue-count{margin-left:8px}
        .update-issue-count:hover{-animation: spin .7s 1 linear;
            -webkit-animation: spin2 .7s 1 linear;}

    </style>

</head>
<body>
<?php
$class = [
    'dashboard_class'=>'','projects_class'=>'active', 'message_class'=>'', 'customers_class'=>'', 'internal_user_class'=>'', 'analytics_class'=>''
];
$this->load->view('common/pm_nav', $class);
?>

<div class="col-md-offset-1 col-md-10">
    <!-- Page Content -->
    <button onclick="location.href='<?=base_url().'projects/create_new_project'?>';" class="fab-button"><i class="fa fa-plus"></i></button>
    <div class="col-md-12">
        <h1 class="page-header" style="margin:30px 0 20px 0">
            <table width="100%" >
                <tr>
                    <td width="79%" rowspan="2">Projects</td>
                    <td width="21%" style="height:auto"><div style="font-size:16px">Urgency Level</div></td>
                </tr>
                <tr>
                    <td height="20">
                        <span class="badge" style="background-color: rgba(200,50,50, 0.7);">High</span>
                        <span class="badge" style="background-color: rgba(250,120,0,0.7);">Moderate</span>
                        <span class="badge" style="background-color: rgba(44,74,215,0.7);">Low</span>
                    </td>

                </tr>
            </table>
        </h1>
        <div class="col-md-12">

            <?php if($this->session->userdata('message')):?>
                <div class="alert alert-info " role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>
                    <?=$this->session->userdata('message')?>
                </div>
                <?php $this->session->unset_userdata('message') ?>
            <?php endif;?>
        </div>
        <ul class="nav nav-pills" style="">
            <li class="active"><a href="#ongoing-panel" aria-controls="ongoing-panel" role="tab" data-toggle="tab" style="padding:5px 10px">Ongoing</a></li>
            <li><a href="#past-panel" aria-controls="past-panel" role="tab" data-toggle="tab" style="padding:5px 10px">Past</a></li>
        </ul>
        <div class="tab-content">
            <div class=" tab-pane fade in active" role="tabpanel" id="ongoing-panel">
                <div class="row">
                <?php foreach($projects as $p): ?>
                <div class="col-lg-3 col-md-4 col-sm-6" style="min-width: 270px;max-width:350px">
                    <div class="panel ongoing-panel project-panel" level="<?=$p["level"]?>" link="<?=base_url().'Projects/view_dashboard/'.$p["project_id"]?>">
                        <div id="project_header<?=$p['project_id']?>" class="panel-heading project-panel-heading"><strong>&nbsp;<?=$p['project_title']?></strong>&nbsp;&nbsp;<p><sub>[<?=$p['project_code']?>]</sub></p></div>
                        <div class="panel-body" style="font-size:15px " >
                            <table class="table table-condensed">
                                <tr>
                                    <td><i class="fa fa-calendar-check-o"></i>&nbsp;<strong>Current Stage </strong></td>
                                    <td><?=$p['phase_name']?$p['phase_name']:"<i>Not Started</i>";?></td>
                                </tr>
                                <tr>
                                    <td> <i class="fa fa-link"></i>&nbsp;<strong>Staging link </strong></td>
                                    <?php if($p['staging_link']):?>
                                        <td class="clickable"> <a href="<?=$p['staging_link']?>" class="clickable" target="_blank"><i class="fa fa-external-link"></i></a></td>
                                    <?php else:?>
                                        <td><i><small>N.A.</small></i></td>
                                    <?php endif;?>
                                </tr>
                                <tr>
                                    <td>  <i class="fa fa-calculator"></i>&nbsp;<strong>Ongoing Issues </strong></td>
                                    <?php
                                    if($p['repo_name_valid']==0) $issue_count = "<i><small>N.A.</small></i>";
                                    else $issue_count = $p['issue_count'];
                                    ?>
                                    <td class="clickable">
                                        <span class="issue-count-<?=$p["project_id"]?>"><?=$issue_count?></span>
                                        <span class="update-issue-count fa fa-rotate-right glyphicon-refresh clickable"></span></td>
                                </tr>
                                <tr>
                                    <td>  <span class="fa fa-fire"></span>&nbsp;<strong>Urgency Score </strong></td>
                                    <td> <?=$p["score"]?></td>
                                </tr>
                            </table>
                            <!--a href="<?=base_url().'Projects/view_dashboard/'.$p["project_id"]?>" class="btn pull-right btn-info"><i class="fa fa-eye"></i> &nbsp;View</a-->

                        </div>
                    </div>
                </div>

            <?php endforeach; //end foreach projects?>
                </div>
            </div>
            <div class=" tab-pane fade" role="tabpanel" id="past-panel" style="">
                <div class="row" id="past-container">
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            $.ajax({
                url:'<?=base_url().'Projects/list_past_projects'?>',
                complete: function(r){ $("#past-container").html(r.responseText);}
            });
            $(".ongoing-panel").on("click",function(event){
                //if it's a left click on non-clickable area in ongoing panel
                if(event.which==1 && !$(event.target).hasClass("clickable") && !$(event.target).is( "a" )){
                    window.location.href = $(this).attr("link");
                }
            });
            $(".clickable").click(function(e) {e.stopPropagation();});
            var refreshIssues = function (){
                var button = $('.update-issue-count');
                button.addClass("glyphicon-refresh-animate");
                $.ajax({
                    url:'<?=base_url()."Scheduled_tasks/fetch_outstanding_issue_counts"?>',
                    success: function(response){
                        var data = jQuery.parseJSON(response);
                        for(var i=0;i<data.length;i++){
                            var id = data[i].id;
                            var count = data[i].count;
                            $(".issue-count-"+id).html(count);
                        }
                        button.removeClass("glyphicon-refresh-animate");
                    }
                });
            };
            if(Math.random()<0.05){refreshIssues()}
            $('.update-issue-count').click(function(){refreshIssues();});
        });
        $(".project-panel[level='High']").each(function(){
            $(this).find('.project-panel-heading').css("background-color","rgba(200,50,50, 0.7)");
            $(this).find('.panel-body').css("background-color","rgba(200,50,50, 0.1)");
        });
        $(".project-panel[level='Moderate']").each(function(){
            $(this).find('.project-panel-heading').css("background-color","rgba(250,120,0,0.7)");
            $(this).find('.panel-body').css("background-color","rgba(250,120,0, 0.1)");
        });
        $(".project-panel[level='Low']").each(function(){
            $(this).find('.project-panel-heading').css("background-color","rgba(44,74,215,0.7)");
            $(this).find('.panel-body').css("background-color","rgba(44,74,215,0.1)");
        });
    </script>

    <!-- /#page-content-wrapper -->

</div>
</body>
</html>