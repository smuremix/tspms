
<?php
defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('common/common_header');?>
    <link rel="stylesheet" href="<?=base_url().'css/sidebar-left.css'?>">
    <script>

        function demoFunction() {
            var x = document.getElementById("estimated_end_time_for_build").value;
            document.getElementById("estimated_end_time_for_build").innerHTML = x;
        }

        $.ajax({
            url: "<?=base_url().'projects/estimate_deadline/'.$project["project_id"]?>",
            //url: "http://localhost/tspms/code/dashboard/get_per_issue_data/1",
            success: function(text){
                $("#estimated_end_time_for_build").val(text);
                $("#estimated_end_time_for_build").attr("auto-date",text)
            },
            error: function(){
                alert("error!");
            }
        });

        $(document).ready(function(){
            $("#Lead" ).click(function() {
                // this change title
                $(".phase").each(function(){
                    $(this).text(" - Lead");
                });
                var project_phase_id=($(this).data().id);
                // this changes updates
                refillUpdates(project_phase_id,"Lead");
                refillMilestones(project_phase_id,"Lead");
            });
            $("#Requirement" ).click(function() {
                // this change title
                $(".phase").each(function(){
                    $(this).text(" - Requirement");
                });
                var project_phase_id=($(this).data().id);
                // this changes updates
                refillUpdates(project_phase_id,"Requirement");
                refillMilestones(project_phase_id,"Requirement");
            });
            $("#Build" ).click(function() {
                // this change title
                $(".phase").each(function(){
                    $(this).text(" - Build");
                });
                var project_phase_id=($(this).data().id);
                // this changes updates
                refillUpdates(project_phase_id,"Build");
                refillMilestones(project_phase_id,"Build");
            });
            $("#Testing" ).click(function() {
                // this change title
                $(".phase").each(function(){
                    $(this).text(" - Testing");
                });
                var project_phase_id=($(this).data().id);
                // this changes updates
                refillUpdates(project_phase_id,"Testing");
                refillMilestones(project_phase_id,"Testing");
            });
            $("#Deploy" ).click(function() {
                // this change title
                $(".phase").each(function(){
                    $(this).text(" - Deploy");
                });
                var project_phase_id=($(this).data().id);
                // this changes updates
                refillUpdates(project_phase_id,"Deploy");
                refillMilestones(project_phase_id,"Deploy");
            });
            $('.clsDatePicker').datepicker({
                dateFormat: 'yy-mm-dd',minDate: '+0d',
                changeMonth: true,changeYear: true,altFormat: "yy-mm-dd",
            });

            $('.clsDatePicker_for_build').datepicker({
                dateFormat: 'yy-mm-dd',minDate: '+0d',
                changeMonth: true,changeYear: true,altFormat: "yy-mm-dd",
                onSelect: function(selectedDate) {
                    // custom callback logic here

                    var auto_date = $("#estimated_end_time_for_build").attr("auto-date");
                    if(auto_date!=null &&auto_date!= undefined){
                        var input_date = new Date(selectedDate);
                        var auto_date = new Date(auto_date);
                        if(input_date<=auto_date){
                            $("#estimated_end_time_for_build").after("<div><br/><font  color='red'>Overloaded! <br/></font>" +
                                "You are might not be able to finish the project in time.<br/>" +
                                "You are suggested to <font  color='red'>change a further date</font> OR <font  color='red'>re-arrange the deadline for other projects in BUILDING phase.</font> </div>")
                        }

                    }


                }
            });

            if($("#timeline").children().length == 0){
                $('.empty-message-update').show();
            }
            if($("#milestone").children().length == 0){
                $('.empty-message-milestone').show();
            }

        });

        function refillUpdates(project_phase_id,phase_name){
            if(project_phase_id!=<?=$project['current_project_phase_id']?>){
                $(".add_button").hide();
            }else{
                $(".add_button").show();
            }
            $("#timeline").text('');
            $(".empty-message-update").hide();
            $.get("<?=base_url('Updates/get_update_by_project_phase/')?>"+'/'+project_phase_id,function(data,status){
                $('#updates-phase').replaceWith('<small>'+phase_name+'</small>');
                var updates = jQuery.parseJSON(data);
                if(updates.length!=0) {
                    $('#timeline').show();
                    updates.forEach(function (element) {
                        var htmlText =
                            '<li><div class="timeline-badge  neutral"></div>' +
                            '<div class="timeline-panel"> <div class="timeline-heading"> <h4 class="timeline-title">' + element.header +
                            <?php if($project['is_ongoing'] == 1){?>
                            '<i class="fa fa-close delete-button" style="cursor: pointer;color:darkgray" onclick="deleteMilestoneButtonClicked(' + element.milestone_id + ')"></i>' +
                            <?php }?>
                            '</h4></div><div class="timeline-body"> <p>' + element.body + '</p> <div class="pull-right timeline-info">' +
                            '<i class="fa fa-user"></i>&nbsp;' + element.posted_by + ' &nbsp;' +
                            '<i class="fa fa-calendar-check-o"></i>&nbsp;' + element.last_updated + '</div>' +
                            ' </div> </div> </li>';

                        $('#timeline').append(htmlText);
                    });
                }else{
                    $('#timeline').hide();
                    $('.empty-message-update').show();

                }
            });

        }

        function refillMilestones(project_phase_id,phase_name){
            $("#milestone").text('');
            $(".empty-message-milestone").hide();
            var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June",
                "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
            ];
            $.get("<?=base_url('Milestones/get_by_project_phase_id/')?>"+'/'+project_phase_id, function(data, status){
                $('#milestones-phase').replaceWith('<small>'+phase_name+'</small>');
                var updates = jQuery.parseJSON(data);
                if(updates.length!=0) {
                    $('#milestone').show();
                    updates.forEach(function (element) {

                        var append;
                        if (element.if_completed == 0) {
                            append = ' <div class="checkbox"> <label> <input type="checkbox" id="done" onchange="completeMilestoneButtonClicked(' + element.milestone_id + ')" > Complete </label> </div>';
                        } else {
                            append = '  <br><span class="badge success" style="background-color: #1ABC9C">Completed</span>';
                        }
                        var ddl = new Date(element.deadline);
                        var day = ddl.getDate();
                        var month = monthNames[ddl.getMonth()];
                        var year = ddl.getFullYear();
                        var htmlText = ' <div class="milestone-item"> <div class=" calendar"> <div class="panel-heading calendar-month">' +
                            '<strong>' + month + '-' + year + '</strong></div><div class="calendar-date">' + day + '</div></div>' +
                            '<div class="milestone-inner"><div class="milestone-header"><strong>' + element.header + '</strong>' +
                            '<i class="fa fa-close delete-button" style="cursor: pointer;color:darkgray" onclick="deleteMilestoneButtonClicked(' + element.milestone_id + ')"></i></div>' +
                            '<div class="milestone-body">' + element.body + '</div>' + append + '</div></div>';

                        $('#milestone').append(htmlText);
                    });
                }else{
                    $('#milestone').hide();
                    $('.empty-message-milestone').show();
                }
            });
        }

        function deleteMilestoneButtonClicked(milestone_id) {
            $('#milestoneDeleteModal').data('milestone_id', milestone_id).modal('show');

        }
        function completeMilestoneButtonClicked(milestone_id) {
            if(document.getElementById('done').checked) {
                $('#milestoneCompletionModal').data('milestone_id', milestone_id).modal('show');
            }
        }
        function deleteUpdateButtonClicked(update_id) {
                $('#updateDeleteModal').data('update_id', update_id).modal('show');
        }
        function confirmUpdateDelete() {
            // handle deletion here
            var uid = $('#updateDeleteModal').data('update_id');
            //to be change to delete update controller
            var delete_u_url = "<?= base_url().'Updates/delete_update/'.$project['project_id'].'/' ?>" + uid;
            window.location.href = delete_u_url;
        }
        function confirmMilestoneComplete() {
            var mid = $('#milestoneCompletionModal').data('milestone_id');
            var complete_m_url = "<?= base_url() . 'Milestones/completionConfirmation/' . $project['project_id'] . '/' ?>" + mid;
           window.location.href = complete_m_url;
        }

        function confirmMilestoneDelete() {
            // handle deletion here
            var mid = $('#milestoneDeleteModal').data('milestone_id');
            //to be change to delete milestone controller
            var delete_m_url = "<?= base_url().'Milestones/delete_milestone/'.$project['project_id'].'/' ?>" + mid;
            window.location.href = delete_m_url;
        }

    </script>
</head>
<body>
<?php
$class = [
    'dashboard_class'=>'', 'projects_class'=>'active', 'message_class'=>'', 'customers_class'=>'', 'internal_user_class'=>'', 'analytics_class'=>''
];
$this->load->view('common/pm_nav', $class);
$this->load->view('common/side_bar', ["_lb_active"=>2,"project"=>$project]);
?>

<div class="content">
<div class="col-md-12">
    <!-- Page Content -->
    <div class="">
        <h1 class="page-header"><?=$project['project_title']?><?php
            if($project['is_ongoing']==1) {
                $if_completed = 1;
                foreach ($milestones as $m) {
                    if ($m['if_completed'] == 0) {
                        $if_completed = 0;
                    }
                }
                $update_phase_button = 'Update Phase';
                if (is_null($next_phase_name)) {
                    $update_phase_button = 'End Project';
                }
            ?>
                <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#updatePhaseModal"><i
                        class="fa fa-pencil-square-o"></i>&nbsp;Update Phase</button>
            <?php } ?>
        </h1>
    </div>
    <div class=" phaseline"><div class="phaseline-inner">
        <div class="phases" style=" transform: translateX(0px);">
            <ol style="list-style: none;">
            <?php
            $current_phase;
            $css_left=-10;
            $css_current_pos = 0.1;
            foreach($phases as $phase){
                $past_project_phase = $phase;
                $css_left += 20;
                $phase_end_time =isset($phase['end_time'])?$phase['end_time']: "now";
                $phase_status = "future";
                if(isset($phase['project_phase_id'])){
                    if(!$phase['phase_id']==0) {//current or past
                        $phase_status = "past";
                        if ($phase['project_phase_id'] == $project['current_project_phase_id']) {//current
                            $current_phase=$phase;
                            $phase_status = "current";
                            $css_current_pos = $css_left/100;
                        }
                    }else{
                        $current_phase=$phase;
                        $phase_status = "current";
                        $css_current_pos = $css_left/100;
                    }

                }
                $selected_tag = $phase_status=="current"? "selected":"";
                echo '<li><a href="#0" data-id="'.$phase['project_phase_id'].'" id="'.$phase['phase_name'].
                    '" data-toggle="tooltip" data-placement="bottom" title="'.$phase['start_time'].' to '.$phase_end_time.
                    '" class="'.$selected_tag. ' '.$phase_status.'" style="left: '.$css_left.'%;"><span>'.$phase['phase_name'].'</span></a></li>';
            }
            if($project['current_project_phase_id']==-1){
                $current_phase = $past_project_phase;
            }
            ?>
            </ol>
                <?php
                if ($project['current_project_phase_id']==-1){
                    $css_current_pos="1.0";
                }
                if ($project['current_project_phase_id']==0){
                    $css_current_pos="0";
                }
                ?>
            <span class="filling-line" aria-hidden="true" style="transform: scaleX(<?=$css_current_pos?>);"></span>
            <script>
                $(".phases a").on("click",function(){
                    $(".phases a").removeClass("selected");$(this).addClass("selected");
                });
            </script>
        </div>
    </div>
    </div>
    <div class="row">

    <?php if(!isset($phases[0]['project_phase_id'])):?>
        <div class="col-xs-12">
            <div class="alert alert-warning" role="alert"><strong>This project hasn't been started. Please update phase to start the project.</strong></div>
        </div>
    <?php else: ?>
    <div class="">
        <div class="col-md-12">
<!-- Message -->
            <?php if($this->session->userdata('message')):?>
                <div class="col-md-12">
                    <div class="alert alert-info " role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                        <?=$this->session->userdata('message')?>
                    </div>
                </div>
                <?php $this->session->unset_userdata('message')?>
            <?php endif;?>
            <div class="col-md-7">
                <h3 style="margin-left:45px">Client Updates<small class="phase"> - <?= $current_phase['phase_name'] ?></small>
                    <?php if($project['is_ongoing']==1) : ?>
                        <button class="add_button btn btn-primary pull-right" data-toggle="modal"
                                data-target="#newUpdateModal"><i class="fa fa-plus"></i>&nbsp; Add
                        </button>
                    <?php endif;?>
                </h3>

                <ul class="timeline" id="timeline" style="<?=$updates?"":"display:none"?>">
                    <?php foreach ($updates as $u): ?>
                    <li><!---Time Line Element--->
                        <div class="timeline-badge  neutral"></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4 class="timeline-title"><?= $u['header'] ?></h4>
                                <i class="fa fa-close delete-button"  style="cursor: pointer;color:darkgray" onclick="deleteUpdateButtonClicked(<?=$u['update_id']?>)"></i>
                            </div>
                            <div class="timeline-body"><!---Time Line Body&Content--->
                                <p><?= $u['body'] ?></p>

                                <div class="pull-right timeline-info">
                                    <i class="fa fa-user"></i>&nbsp;<?= $u['posted_by'] ?> &nbsp;
                                    <i class="fa fa-calendar-check-o"></i>&nbsp;<?= $u['last_updated'] ?></div>
                            </div>
                        </div>
                    </li>
                    <?php endforeach;?>

                </ul>
                <div class="alert alert-info empty-message-update" role="alert" style="<?=!$updates?"":"display:none"?>; margin-top: 30px;">No Update in this phase</div>

            </div>
            <div class="col-md-4">
                <h3 style="margin-bottom:30px">Milestones<small class="phase"> - <?= $current_phase['phase_name'] ?></small>
                    <?php if($project['is_ongoing']==1) :?>
                        <button class="add_button btn btn-primary add-milestone pull-right" data-toggle="modal"
                                data-target="#newMilestoneModal"><i class="fa fa-plus"></i>&nbsp; Add
                        </button>
                    <?php endif;?>
                </h3>
                <div id="milestone">
                    <?php foreach ($milestones as $m) {
                        $monthName = date('M', strtotime($m['deadline']));
                        $dateNumber = date('j', strtotime($m['deadline']));
                        $year = date('Y', strtotime($m['deadline']));
                        ?>
                        <div class="milestone-item">
                            <div class=" calendar">
                                <div class="panel-heading calendar-month">
                                    <strong><?= $monthName . "-" . $year ?></strong></div>
                                <div class="calendar-date"><?= $dateNumber ?></div>
                            </div>
                            <div class="milestone-inner">
                                <div class="milestone-header"><strong><?= $m['header'] ?></strong>
                                    <?php if($project['is_ongoing']==1) : ?>
                                    <i class="fa fa-close delete-button" style="cursor: pointer;color:darkgray" onclick="deleteMilestoneButtonClicked(<?= $m['milestone_id'] ?>)"></i>
                                    <?php endif;?>
                                </div>
                                <div class="milestone-body"><?= $m['body'] ?></div>
                                <?php if ($m['if_completed'] == 0) :?>
                                <div class="checkbox">
                                    <label><input type="checkbox" id="done" onchange="completeMilestoneButtonClicked(<?=$m['milestone_id']?>)" > Complete</label>
                                </div>
                                <?php else : ?>
                                <span class="badge success" style="background-color:#1ABC9C">Completed</span>
                                <?php endif;?>

                            </div>
                        </div>
                    <?php  } //foreach milestone?>
                </div>
                <div class="alert alert-info empty-message-milestone" role="alert" style="<?=!$milestones?"":"display:none"?>">No Milestone in this phase</div>
                </div>
        </div>
    </div>
    <?php endif;//project started ?>
    </div>
    <!-- /#page-content-wrapper -->

</div>
<!--new update modal-->
<div class="modal fade" id="newUpdateModal" data-parsley-validate tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" >New Update</h4>
            </div>

            <form role="form" action="<?=base_url().'Updates/add_new_update/'.$project['project_id'].'/'.$project['current_project_phase_id']?>" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="title">Title:</label>
                        <input name="update_header" type="text" class="form-control"  id="update_title" data-parsley-required>
                    </div>
                    <div class="form-group">
                        <label for="description">Content:</label>
                        <textarea name="update_body" class="form-control" rows="4" id="description" data-parsley-required></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <input type="submit" name="submit" id="submit" class="btn btn-primary" value="Submit">
                </div>
            </form>

        </div>
    </div>
</div>
<!--new milestone modal-->
<div class="modal fade" id="newMilestoneModal" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" >New Milestone</h4>
            </div>

            <form id="newMilestone" data-parsley-validate role="form" action="<?=base_url().'Milestones/add_new_milestone/'.$project['project_id'].'/'.$project['current_project_phase_id']?>" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="title">Title:</label>
                        <input name="header" type="text" class="form-control"  id="milestone_title" data-parsley-required>
                    </div>
                    <div class="form-group">
                        <label for="title">Deadline:</label>
                        <!--div class="input-group"-->
                            <input type="text" name="deadlinePicker" id="deadlinePicker" class="form-control clsDatePicker" data-parsley-required>
                            <!--span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div-->
                    </div>
                    <div class="form-group">
                        <label for="milestone_description">Description:</label>
                        <textarea name="body" class="form-control" rows="4" id="milestone_description" data-parsley-required></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <input type="submit" name="submit" id="submit" class="btn btn-primary" value="Submit">
                </div>
            </form>
        </div>
    </div>
</div>
<!--update phase modal-->
<div class="modal fade" id="updatePhaseModal" tabindex="-1" role="dialog" >

    <div class="modal-dialog" role="document">
        <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Update Phase</h4>
                </div>

                    <div class="modal-body">
                        <div class="tabbable">
                            <ul class="nav nav-tabs">
                                <?php
                                    if(isset($current_phase)&&$current_phase['phase_id']==5) {
                                        ?>
                                        <li><a href="#endProject" data-toggle="tab">End Project</a></li>
                                        <li class="active"><a href="#changeEstEndDate" data-toggle="tab">Update Est.End Date</a></li>
                                        <?php
                                    }else {
                                        ?>
                                        <li class="active"><a href="#nextPhase" data-toggle="tab">Next Phase</a></li>
                                        <li><a href="#changeEstEndDate" data-toggle="tab">Update Est.End Date</a></li>
                                        <?php
                                    }
                                ?>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="nextPhase">
                                    <?php
                                    $uncompletedMilestone = 0;
                                    foreach($milestones as $m){
                                        if($m['if_completed']==0){
                                            $uncompletedMilestone = 1;
                                        }
                                    }
                                    if($uncompletedMilestone==1){
                                        ?>
                                        <h4>Unable to update phase!</h4>
                                        <h5>You still have uncompleted milestones in this phase. Please complete them before updaing phase.</h5>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                        </div>
                                    <?php
                                    }else {
                                        ?>
                                        <form role="form" data-parsley-validate
                                              action="<?= base_url() . 'Project_phase/update_phase/' . $project["project_id"] . '/' . $project['current_project_phase_id'] ?>"
                                              method="post">
                                            <div class="form-group">
                                                <label for="title">Title:</label>
                                                <input type="hidden" name="tab" value="nextPhase">
                                                <input type="text" class="form-control" disabled
                                                       value="<?= $next_phase_name ?>" id="title">
                                            </div>
                                            <div class="form-group">
                                                <label for="estimated_end_time">Estimated End Date:</label>
                                                <!--div class="input-group"-->
                                                <?php

                                                    if(isset($current_phase) && $current_phase['phase_id']==2){

                                                        ?>
                                                        <br/>Default value is estimated based on the Use Case Point forecast
                                                        <input type="text" name="estimated_end_time" onkeyup="demoFunction()"
                                                               id="estimated_end_time_for_build"
                                                               class="form-control clsDatePicker_for_build" data-parsley-required>


                                                        <?php
                                                    } else {
                                                        ?>
                                                        <input type="text" name="estimated_end_time"
                                                               id="estimated_end_time"
                                                               class="form-control clsDatePicker" data-parsley-required>
                                                        <?php
                                                    }
                                                ?>

                                                <!--span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                            </div-->
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">
                                                    Cancel
                                                </button>
                                                <input type="submit" name="submit" id="submit" class="btn btn-primary"
                                                       value="Update">
                                            </div>
                                        </form>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <div class="tab-pane" id="changeEstEndDate">
                                    <form role="form" data-parsley-validate action="<?=base_url().'Project_phase/update_est_end_date/'.$project["project_id"].'/'.$project['current_project_phase_id']?>" method="post">
                                    <?php
                                    if(!isset($current_phase)){
                                        ?>
                                        <h4>Unable to update estimated end date!</h4>
                                        <h5>You haven't started this project.</h5>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                        </div>
                                    <?php
                                    }else {
                                        ?>
                                        <div class="form-group">
                                            <label for="title">Title:</label>
                                            <input type="hidden" name="tab" value="changeEstEndDate">
                                            <input type="text" class="form-control" disabled
                                                   value="<?= $current_phase['phase_name'] ?>" id="title">
                                        </div>
                                        <div class="form-group">
                                            <label for="new_estimated_end_date">New Estimated End Date:</label>
                                            <!--div class="input-group"-->
                                            <input type="text" name="new_estimated_end_date" id="new_estimated_end_date"
                                                   class="form-control clsDatePicker" data-parsley-required>
                                            <!--span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                        </div-->
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                            <input type="submit" name="submit" id="submit" class="btn btn-primary" value="Update">
                                        </div>
                                        <?php
                                        }
                                    ?>
                                    </form>
                                </div>
                                <div class="tab-pane" id="endProject">
                                    <form role="form" data-parsley-validate action="<?=base_url().'Project_phase/end_project/'.$project["project_id"].'/'.$project['current_project_phase_id']?>" method="post">
                                    <div class="form-group">
                                        <input type="hidden" name="tab" value="endProject">
                                            <h4>This project will be ended. You can still find it
                                                in "Past Projects".</h4>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                        <input type="submit" name="submit" id="submit" class="btn btn-primary" value="End Project">
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

            </div>
        </div>
    </div>
</div>
<!--update phase alert modal-->
<div class="modal fade" id="update_phase_alert_modal" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Unable to update phase!</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="title">You haven't completed all milestones of current phase.</label>
                        <br>
                    <p> Plase make sure that you have completed all the milestones of current phase before updating.</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!--Milestone Completion Modal-->


<div class="modal fade" id="milestoneCompletionModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <strong>Complete Milestone</strong>
                </div>
                    <div class="modal-body">
                       Do you wish to complete this milestone?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="cancelComplete()">Cancel</button>
                        <input type="submit" name="submit" id="submit" class="btn btn-success" onclick="confirmMilestoneComplete()" value="Complete">
                    </div>
            </div>
        </div>
    </div>
<!--Milestone Delete Modal-->
<div class="modal fade" id="milestoneDeleteModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <strong>Delete Milestone</strong>
                </div>
                    <div class="modal-body">
                        This action cannot be undone, do you wish to proceed?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-danger" id="btnYes" onclick="confirmMilestoneDelete()"> Delete </button>
                    </div>
            </div>
        </div>
    </div>
<!--Update Delete Modal-->
<div class="modal fade" id="updateDeleteModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <strong>Delete Update</strong>
            </div>
            <div class="modal-body">
                This action cannot be undone, do you wish to proceed?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger" id="btnYes" onclick="confirmUpdateDelete()">Delete</button>
            </div>
        </div>
    </div>
</div>



</body>
</html>