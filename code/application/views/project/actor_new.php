<?php
defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('common/common_header');?>
    <link rel="stylesheet" href="<?=base_url().'css/sidebar-left.css'?>">
</head>
<body >
<?php
$class = [
    'dashboard_class'=>'',
    'projects_class'=>'active',
    'message_class'=>'',
    'customers_class'=>'',
    'internal_user_class'=>'',
    'analytics_class'=>''
];
$this->load->view('common/pm_nav', $class);
//$this->load->view('common/side_bar', ["_lb_active"=>6,"project"=>$project]);
?>

<div class="container content">
    <h1 class="page-header">
        New Actor&nbsp;
        <a href="<?= base_url() . 'Usecases/list_all/'.$project['project_id'] ?>" class="btn btn-primary"><i class="fa fa-backward"></i>&nbsp;Back</a>
    </h1>
    <form  data-parsley-validate role="form" action="<?=base_url().'Usecases/new_actor/'.$project['project_id']?>" method="post">
        <div class="col-md-12">
            <div class="form-group">
                <label for="actor_name">Actor Name*:</label>
                <input name="actor_name" id="actor_name"  type="actor_name" class="form-control" data-parsley-required>
            </div>
            <div class="form-group">
                <label for="flow">Description:</label>
                <textarea class="form-control" name="flow" id="flow" rows="3" ></textarea>
            </div>

        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="classification">Classification:</label>
                <select class="form-control" id="classification" name="classification" data-parsley-required>
                    <option value="Simple">Simple</option>
                    <option value="Average">Average</option>
                    <option value="Complex">Complex</option>
                </select>
            </div>
        </div>

        <div class="col-md-12 pull-right">
            <input type="submit" name="submit" id="submit" class="btn btn-primary" value="submit">
            <!--<a href="//?base_url().'Customers/edit/'.$c["c_id"]?" class="btn btn-primary">Submit</a>-->
            <a href="<?= base_url() . 'Usecases/list_all/'.$project['project_id'] ?>" class="btn btn-default">Cancel</a>
        </div>


    </form>

</div>

</body>
</html>