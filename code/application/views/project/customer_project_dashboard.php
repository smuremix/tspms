<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('common/common_header');?>
    <link href="<?=base_url().'css/timeline.css'?>" rel="stylesheet">
    <link rel="stylesheet" href="<?=base_url().'css/sidebar-left.css'?>">

    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });
        $(document).ready(function(){
            $("#Lead" ).click(function() {
                // this change title
                $(".phase").each(function(){
                    $(this).text(" - Lead");
                });
                var project_phase_id=($(this).data().id);
                // this changes updates
                refillUpdates(project_phase_id);
                refillMilestones(project_phase_id);
            });
            $("#Requirement" ).click(function() {
                // this change title
                $(".phase").each(function(){
                    $(this).text(" - Requirement");
                });
                var project_phase_id=($(this).data().id);
                // this changes updates
                refillUpdates(project_phase_id);
                refillMilestones(project_phase_id);
            });
            $("#Build" ).click(function() {
                // this change title
                $(".phase").each(function(){
                    $(this).text(" - Build");
                });
                var project_phase_id=($(this).data().id);
                // this changes updates
                refillUpdates(project_phase_id);
                refillMilestones(project_phase_id);
            });
            $("#Testing" ).click(function() {
                // this change title
                $(".phase").each(function(){
                    $(this).text(" - Testing");
                });
                var project_phase_id=($(this).data().id);
                // this changes updates
                refillUpdates(project_phase_id);
                refillMilestones(project_phase_id);
            });
            $("#Deploy" ).click(function() {
                // this change title
                $(".phase").each(function(){
                    $(this).text(" - Deploy");
                });
                var project_phase_id=($(this).data().id);
                // this changes updates
                refillUpdates(project_phase_id);
                refillMilestones(project_phase_id);
            });

        });

        function refillUpdates(project_phase_id){
            $("#timeline").text('');
            $.get("<?=base_url('updates/get_update_by_project_phase/')?>"+'/'+project_phase_id, function(data, status){
                var updates = jQuery.parseJSON(data);
                updates.forEach(function(element){
                    var htmlText = '<li>'+
                        '<div class="timeline-badge  neutral"></div>'+
                        '<div class="timeline-panel"> <div class="timeline-heading"> <h4 class="timeline-title">'+element.header+'</h4> </div>'+
                        '<div class="timeline-body"> <p>'+element.body+'</p> <div class="pull-right timeline-info">'+
                        '<i class="fa fa-user"></i>&nbsp;'+element.posted_by+' &nbsp;'+
                        '<i class="fa fa-calendar-check-o"></i>&nbsp;'+element.last_updated+'</div>'+
                        ' </div> </div> </li>';
                    $('#timeline').append( htmlText );
                });
            });
        }

        function refillMilestones(project_phase_id){
            $("#milestone").text('');
            var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];
            $.get("<?=base_url('milestones/get_by_project_phase_id/')?>"+'/'+project_phase_id, function(data, status){
                var updates = jQuery.parseJSON(data);
                updates.forEach(function(element){
                    var ddl=new Date(element.deadline);
                    var day = ddl.getDate();
                    var month=monthNames[ddl.getMonth()];
                    var year=ddl.getFullYear();
                    var append='';
                    if (element.if_completed!=0){
                        append='<br><span class="badge success" style="background-color: #1ABC9C">Completed</span>';
                    }else{
                        append='';
                    }
                    var htmlText = '<div class="milestone-item"><div class=" calendar">' +
                        '<div class="panel-heading calendar-month"><strong>'+month+'-'+year+'</strong></div>'+
                        '<div class="calendar-date">'+day+'</div> </div><div class="milestone-inner"><div class="milestone-header">'+
                        '<strong>'+element.header+'</strong></strong></div><div class="milestone-body">'+
                        ''+element.body+"</div>"+append+ ' </div> </div>';
                    $('#milestone').append( htmlText );
                });
            });
        }

    </script>
</head>
<body>
<?php
$class = [ 'projects_class'=>'active', 'message_class'=>''];
$this->load->view('common/customer_nav', $class);
$this->load->view('common/side_bar_cust', ["_lb_active"=>1,"project_id"=>$project_id]);
?>

<div class="content">
    <!-- Page Content -->
    <div class="col-md-12">
        <h1 class="page-header">
           <?=$project['project_title']?>&nbsp;
            <?php if($project['customer_preview_link']):?>
            <a href="<?=$project['customer_preview_link']?>" class="btn btn-info" target="_blank"><i class="fa fa-external-link"></i>&nbsp;Prototype</a>
            <?php endif?>
        </h1>
        <h4 style="color:darkgrey">Click each phase on timeline to check updates for each phase.</h4>


    <!-- /.row -->
    <div class=" phaseline">
        <div class="phaseline-inner">
        <div class="phases" style=" transform: translateX(0px);">
            <ol style="list-style: none;">
                <?php
                $current_phase;
                $css_left=-10;
                $css_current_pos = 0.1;
                foreach($phases as $phase){
                    $past_project_phase = $phase;
                    $css_left += 20;
                    $phase_end_time =isset($phase['end_time'])?$phase['end_time']: "now";
                    $phase_status = "future";
                    if(isset($phase['project_phase_id'])){
                        if(!$phase['phase_id']==0) {//current or past
                            $phase_status = "past";
                            if ($phase['project_phase_id'] == $project['current_project_phase_id']) {//current
                                $current_phase=$phase;
                                $phase_status = "current";
                                $css_current_pos = $css_left/100;
                            }
                        }else{
                            $current_phase=$phase;
                            $phase_status = "current";
                            $css_current_pos = $css_left/100;
                        }

                    }
                    $selected_tag = $phase_status=="current"? "selected":"";
                    echo '<li><a href="#0" data-id="'.$phase['project_phase_id'].'" id="'.$phase['phase_name'].
                        '" data-toggle="tooltip" data-placement="bottom" title="'.$phase['start_time'].' to '.$phase_end_time.
                        '" class="'.$selected_tag. ' '.$phase_status.'" style="left: '.$css_left.'%;"><span>'
                        .$phase['phase_name'].'</span></a></li>';
                }
                if($project['current_project_phase_id']==-1){
                    $current_phase = $past_project_phase;
                }
                ?>
            </ol>
            <?php
            if ($project['current_project_phase_id']==-1){
                $css_current_pos="1.0";
            }
            if ($project['current_project_phase_id']==0){
                $css_current_pos="0";
            }
            ?>
            <span class="filling-line" aria-hidden="true" style="transform: scaleX(<?=$css_current_pos?>);"></span>
            <script>
                $(".phases a").on("click",function(){
                    $(".phases a").removeClass("selected");$(this).addClass("selected");
                });
            </script>
        </div>
    </div>
    </div>
    <?php if(!isset($current_phase)) : ?>
        <div class="col-xs-12">
            <div class="alert alert-warning" role="alert"><strong>This project hasn't started.</strong></div>
        </div>
    <?php else: ?>
        <div class="row">

            <div class="col-md-12">
                <div class="col-md-7">
                    <h3 style="margin-left:45px">Client Updates<small class="phase"> - <?= $current_phase['phase_name'] ?></small></h3>
                    <ul class="timeline" id="timeline" style="">
                        <?php foreach ($updates as $u): ?>
                            <li><!---Time Line Element--->
                                <div class="timeline-badge  neutral"></div>
                                <div class="timeline-panel">
                                    <div class="timeline-heading">
                                        <h4 class="timeline-title"><?= $u['header'] ?></h4>
                                    </div>
                                    <div class="timeline-body"><!---Time Line Body&Content--->
                                        <p><?= $u['body'] ?></p>

                                        <div class="pull-right timeline-info">
                                            <i class="fa fa-user"></i>&nbsp;<?= $u['posted_by'] ?> &nbsp;
                                            <i class="fa fa-calendar-check-o"></i>&nbsp;<?= $u['last_updated'] ?></div>
                                    </div>
                                </div>
                            </li>
                        <?php endforeach;?>

                    </ul>
                </div>
                <div class="col-md-4">
                    <h3 style="margin-bottom:30px">Milestones<small class="phase"> - <?= $current_phase['phase_name'] ?></small></h3>
                    <div id="milestone">
                        <?php foreach($milestones as $m):
                            $month = date('M',strtotime($m['deadline']));
                            $date =  date('d',strtotime($m['deadline']));
                            $year =  date('Y',strtotime($m['deadline']));
                        ?>
                    <div class="milestone-item">
                        <div class=" calendar">
                            <div class="panel-heading calendar-month">
                                <strong><?= $month . "-" . $year ?></strong></div>
                            <div class="calendar-date"><?= $date ?></div>
                        </div>
                        <div class="milestone-inner">
                            <div class="milestone-header"><strong><?= $m['header'] ?></strong></div>
                            <div class="milestone-body"><?= $m['body'] ?></div>
                            <?php if ($m['if_completed'] == 0) :?>
                                <div class="checkbox">
                                    <label><input type="checkbox" id="done" onchange="completeMilestoneButtonClicked(<?=$m['milestone_id']?>)" > Complete</label>
                                </div>
                            <?php else : ?>
                                <span class="badge success" style="background-color:#1ABC9C">Completed</span>
                            <?php endif;?>

                        </div>
                    </div>
                        <?php endforeach?>
                </div>
                </div>
            </div>


        </div>
    <?php endif;?>
    </div>
</div>

</body>
</html>