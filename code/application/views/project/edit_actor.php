<?php
defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('common/common_header');?>
    <link rel="stylesheet" href="<?=base_url().'css/sidebar-left.css'?>">
</head>
<body >

<?php
$class = [
    'dashboard_class'=>'',
    'projects_class'=>'active',
    'message_class'=>'',
    'customers_class'=>'',
    'internal_user_class'=>'',
    'analytics_class'=>''
];
$this->load->view('common/pm_nav', $class);

$this->load->view('common/side_bar', ["_lb_active"=>4,"project"=>$project]);
?>


<div class="container content">
    <h1 class="page-header">
        Edit Actor &nbsp;
        <a href="<?= base_url() . 'Usecases/list_all/'.$project['project_id'] ?>" class="btn btn-primary"><i class="fa fa-backward"></i>&nbsp;Back</a>
    </h1>
    <form  data-parsley-validate role="form" action="<?=base_url().'Usecases/edit_actor/'.$actor['actor_id']?>" method="post">
        <div class="col-md-12">
            <div class="form-group">
                <label for="actor_name">Title*:</label>
                <input name="actor_name" id="actor_name"  type="text" class="form-control" value="<?=$actor['actor_name']?>" data-parsley-required>
            </div>
            <div class="form-group">
                <label for="description">Description:</label>
                <textarea class="form-control" name="description" id="description" rows="3" ><?=$actor['description']?></textarea>
                <script>
                    CKEDITOR.replace( 'description' );
                </script>
            </div>

        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="classification">Classification:</label>
                <select class="form-control" id="classification" name="classification" data-parsley-required>
                    <option value="Simple" <?=set_select("classification","Simple",$actor['classification']=="Simple")?>>Simple</option>
                    <option value="Average" <?=set_select("classification","Average",$actor['classification']=="Average")?>>Average</option>
                    <option value="Complex"<?=set_select("classification","Complex",$actor['classification']=="Complex")?>>Complex</option>
                </select>
            </div>
        </div>

        <div class="col-md-12 pull-right">
            <input type="submit" name="submit" id="submit" class="btn btn-primary" value="Submit">
            <!--<a href="//?base_url().'Customers/edit/'.$c["c_id"]?" class="btn btn-primary">Submit</a>-->
            <a href="<?= base_url() . 'Usecases/list_all/'.$project['project_id'] ?>" class="btn btn-default">Cancel</a>
        </div>


    </form>

</div>



</body>

</body>
</html>