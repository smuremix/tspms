
<?php
defined('BASEPATH') OR exit('No direct script access allowed');?>
        <?php
        if(sizeof($projects)==0){
        ?>
            <div class="alert alert-warning" role="alert"><strong>There is no past project yet.</strong></div>
        <?php
        }else{
            foreach($projects as $p){
                ?>
                <div class=" col-md-4">
                    <div class="panel past-panel project-panel">
                        <div class="panel-heading" style="text-align:center" ><strong><?=$p['project_title']?></strong></div>
                        <div class="panel-body" style="font-size:15px" >
                            <table class="table table-condensed">
                                <tr>
                                    <td> <i class="fa fa-link"></i>&nbsp;<strong>Production link </strong></td>
                                    <td> <a href="<?=$p['production_link']?>" target="_blank">Click here</a></td>
                                </tr>
                            </table>

                            <a href="<?=base_url().'Projects/view_dashboard/'.$p["project_id"]?>" class="btn pull-right btn-info"><i class="fa fa-eye"></i> &nbsp;View</a>

                        </div>
                    </div>
                </div>
                <?php
            }
        }
        ?>
