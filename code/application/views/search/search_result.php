<?php
defined('BASEPATH') OR exit('No direct script access allowed');?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <?php $this->load->view('common/common_header');?>
        <link rel="stylesheet" href="<?=base_url().'css/sidebar-left.css'?>">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css">
        <script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/moment.js/1.7.2/moment.min.js"></script>
        <script>

        </script>
    </head>
    <body >
<?php
$class = [
    'dashboard_class'=>'',
    'projects_class'=>'',
    'message_class'=>'',
    'customers_class'=>'',
    'internal_user_class'=>'',
    'analytics_class'=>''
];
$this->load->view('common/pm_nav', $class);
?>
    <div class="col-md-10 content">
        <div class="col-md-12">
            <h1 class="page-header">
                 Search Results - <small>"<?=$key?>"</small>
                <form class="navbar-form" role="search" method="get" action="<?=base_url('Search')?>">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search" name="key" required>
                        <div class="input-group-btn">
                            <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                        </div>
                    </div>
                </form>
            </h1>

        </div>
      <?php if(empty($chats)&&empty($tasks)&&empty($milestones)&&empty($updates)&empty($files)){?>
            <h4>Sorry, there is no record matches your search term...</h4>
        <?php }else{?>
        <ul class="nav nav-tabs">
            <li id="chat" class="active" onclick="loadChat()"><a href="#">Chats
                    <?php
                    if(!empty($chats)){
                        echo '<span class="badge" style="background-color: darkslategrey">'.sizeof($chats).'</span>';
                    }
                    ?>
            </a></li>
            <li id="task"><a href="#" onclick="loadTask()">Tasks
                    <?php
                    if(!empty($tasks)){
                        echo '<span class="badge" style="background-color: darkslategrey">'.sizeof($tasks).'</span>';
                    }
                    ?>
            </a></li>
            <li id="milestone"><a href="#" onclick="loadMilestone()">Milestones
                    <?php
                    if(!empty($milestones)){
                        echo '<span class="badge" style="background-color: darkslategrey">'.sizeof($milestones).'</span>';
                    }
                    ?>
            </a></li>
            <li id="update"><a href="#" onclick="loadUpdate()">Updates
                    <?php
                    if(!empty($updates)){
                        echo '<span class="badge" style="background-color: darkslategrey">'.sizeof($updates).'</span>';
                    }
                    ?>
            </a></li>
            <li id="file"><a href="#" onclick="loadFile()">Files
                    <?php
                    if(!empty($files)){
                        echo '<span class="badge" style="background-color: darkslategrey">'.sizeof($files).'</span>';
                    }
                    ?>
            </a></li>
        </ul>
    <div id="frame">
    <?php if(empty($chats)){?>
        <h4>Sorry, there is no chat message matches your search term...</h4>
    <?php }else{ ?>
        <table id="data" class="table table-striped">
            <thead>
            <th>From/To</th>
            <th>Content</th>
            <th>Time</th>
            <th>Context</th>
            </thead>
            <?php foreach ($chats as $c) {
                $epoch = $c['time_created'];
                $dt = new DateTime("@$epoch");
                echo '<tr>';
                if ($c['to_pm'] == 1) {
                    echo '<td>From ' . $c['user1'] . '</td>';
                } else {
                    echo '<td>To ' . $c['user1'] . '</td>';
                }
                if ($c['link'] == '0') {
                    echo '<td>' . $c['body'] . '</td>';
                } else {
                    echo '<td> <a href="' . $c['link'] . '"> ' . $c['file_name'] . '</a></td>';
                }
                echo '<td>' . $dt->format('Y-m-d H:i:s') . '</td> <td><i class="fa fa-eye" onclick=loadChatThread('.$c["message_id"].','.$c["time_created"].')></i></td></tr>';
            }
        }
            //var_dump($tasks);
        ?>

    </div>
        <?php }?>

    </div>
<!-- Modal -->
<div id="contextModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="title">Chat context(up to 10)</h4>
            </div>
            <div class="modal-body" id="chatContext" style="margin-right: 5px; margin-left: 5px">



            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
    </body>
    <script>
        function loadTask(){
            $('#chat').removeClass();
            $('#milestone').removeClass();
            $('#update').removeClass();
            $('#file').removeClass();
            $('#task').addClass('active');
            $('#frame').text(" ");
            var text='';
            <?php if(empty($tasks)){?>
            text = ' <h4>Sorry, there is no task matches your search term...</h4>';
            <?php }else{?>
            text=' <table id="data" class="table table-striped"> <thead><th>Project Code</th> <th>Content</th> <th>View</th></thead><tbody>';
            <?php foreach($tasks as $t):?>
            //console.log('hello');
            var url= '<?=base_url('Projects/view_dashboard/'.$t['project_id'])?>';
            text=text+'<tr><td>'+ '<?=$t['project_code']?>'+'</td><td>'+ '<?=addslashes($t['content'])?>'+'</td> <td><a href="' + url +'">'+' View</a></td></tr>';
            <?php endforeach ?>
           text = text +'</tbody></table>';
           <?php }?>
            $('#frame').append( text );
            $('#data').dataTable(
                {
                    "bFilter": false,
                    "bInfo": false,
                    "bLengthChange": false,
                    "pageLength": 15
                }
            );

        }

        function loadChat(){
            $('#task').removeClass();
            $('#milestone').removeClass();
            $('#update').removeClass();
            $('#file').removeClass();
            $('#chat').addClass('active');
            $('#frame').text(" ");
            var text='';
            <?php if(empty($chats)){?>
            text = ' <h4>Sorry, there is no chat message matches your search term...</h4>';
            <?php }else{?>
                text=' <table id="data" class="table table-striped"> <thead> <th>From/To</th> <th>Content</th> <th>Time</th><th>context</th> </thead>';
                <?php foreach($chats as $c):
                    $epoch = $c['time_created'];
                    $dt = new DateTime("@$epoch");
                    if ($c['to_pm'] == 1) {?>
                    text=text+'<tr> <td>From ' + '<?=$c['user1']?>' + '</td>';
                    <?php }else{?>
                    text=text+'<tr><td>To ' + '<?=$c['user1']?>' + '</td>';
                    <?php }?>

                    <?php if ($c['link'] == '0') {?>
                        text=text+'<td>' +'<?=addslashes($c['body'])?>' + '</td>';
                    <?php }else{?>
                        text=text+'<td> <a href="' +'<?= $c['link']?>'+ '"> '+ '<?=$c['file_name'] ?>'+'</a></td>';
                    <?php }?>
                     text = text +'<td>' + '<?=$dt->format('Y-m-d H:i:s')?>' + '</td>' +
                     '<td><i class="fa fa-eye" onclick="loadChatThread('+ <?=$c["message_id"]?>+','+ <?=$c["time_created"]?>+')"></i></td>' +
                     '</tr>';
            <?php endforeach ?>
                text = text+'</table>';
            <?php }?>
            $('#frame').append( text );
            $('#data').dataTable(
                {
                    "bFilter": false,
                    "bInfo": false,
                    "bLengthChange": false,
                    "pageLength": 15
                }
            );

        }

        function loadUpdate(){
            $('#chat').removeClass();
            $('#milestone').removeClass();
            $('#task').removeClass();
            $('#file').removeClass();
            $('#update').addClass('active');
            $('#frame').text(" ");
            var text='';
            <?php if(empty($updates)){?>
            text = ' <h4>Sorry, there is no update matches your search term...</h4>';
            <?php }else{?>
            text=' <table id="data" class="table table-striped"> <thead><th>Project Code</th> <th>Content</th> <th>Time</th></thead>';
            <?php foreach($updates as $u):?>
            var url= '<?=base_url('Projects/view_updates/'.$u['project_id'])?>';
            text=text+'<tr><td><a href="' + url +'">'+ '<?=$u['project_code']?>'+'</a></td> <td>'+'<?=addslashes($u['body'])?>'+'</td> <td>'+ '<?=$u['last_updated']?>' +'</td></tr>';
            <?php endforeach ?>
            text = text +'</table>';
            <?php }?>
            $('#frame').append( text );
            $('#data').dataTable(
                {
                    "bFilter": false,
                    "bInfo": false,
                    "bLengthChange": false,
                    "pageLength": 15
                }
            );

        }

        function loadMilestone(){
            $('#chat').removeClass();
            $('#update').removeClass();
            $('#file').removeClass();
            $('#task').removeClass();
            $('#milestone').addClass('active');
            $('#frame').text(" ");
            var text='';
            <?php if(empty($milestones)){?>
            text = ' <h4>Sorry, there is no milestone matches your search term...</h4>';
            <?php }else{?>
            text=' <table id="data" class="table table-striped"> <thead><th>Project Code</th> <th>Title</th> <th>Content</th></thead>';
            <?php foreach($milestones as $m):?>
            var url= '<?=base_url('Projects/view_updates/'.$m['project_id'])?>';
            text=text+'<tr><td> <a href="' + url +'">'+ '<?=$m['project_code']?>'+'</a></td> <td>'+'<?=addslashes($m['header'])?>'+'</td> <td>'+ '<?=addslashes($m['body'])?>' +'</td></tr>';
            <?php endforeach ?>
            text = text +'</table>';
            <?php }?>
            $('#frame').append( text );
            $('#data').dataTable(
                {
                    "bFilter": false,
                    "bInfo": false,
                    "bLengthChange": false,
                    "pageLength": 15
                }
            );


        }
        function loadChatThread(mid, mtime){
            $("#chatContext").text('');
            $.get("<?=base_url('chat/get_context/')?>"+'/'+mid,function(data,status){
                var chats = jQuery.parseJSON(data);
                chats.forEach(function(element){

                    var htmlText = "";

                    if (element.message_id== mid){

                        if(element.is_file==0){
                            htmlText = '<hr><div class ="row"><span style="background-color: #FFFF00"><strong>'+element.user1+'&nbsp;&nbsp;'+  moment.unix(element.time_created).format(" YYYY-MM-DD HH:mm:ss") +'</strong></span></div>'+
                            '<div class ="row">'+element.content+'</div>'
                        }else{
                            htmlText = '<hr><div class ="row"><span style="background-color: #FFFF00"><strong>'+element.user1+'&nbsp;&nbsp;'+  moment.unix(element.time_created).format(" YYYY-MM-DD HH:mm:ss") +'</strong></span></div>'+
                            '<div class ="row"><a href="'+ element.link+ '">'+ element.file_name +'</a></div>'
                        }
                    }else{
                        if(element.is_file==0) {
                            if (element.to_pm == 0) {
                                htmlText = '<hr><div class ="row"><strong>' + element.user1 + '&nbsp;&nbsp;' + moment.unix(element.time_created).format(" YYYY-MM-DD HH:mm:ss") + '</strong></div>' +
                                '<div class ="row">' + element.content + '</div>'
                            } else {
                                htmlText = '<hr><div class ="row"><strong>You&nbsp;</strong>' + moment.unix(element.time_created).format(" YYYY-MM-DD HH:mm:ss") + '</div>' +
                                '<div class ="row">' + element.content + '</div>'
                            }
                        }else{
                            if (element.to_pm == 0) {
                                htmlText = '<hr><div class ="row"><strong>' + element.user1 + '&nbsp;&nbsp;' + moment.unix(element.time_created).format(" YYYY-MM-DD HH:mm:ss") + '</strong></div>' +
                                '<div class ="row"><a href="'+ element.link+ '">'+ element.file_name +'</a></div>'
                            } else {
                                htmlText = '<hr><div class ="row"><strong>You&nbsp;</strong>' + moment.unix(element.time_created).format(" YYYY-MM-DD HH:mm:ss") + '</div>' +
                                '<div class ="row"><a href="'+ element.link+ '">'+ element.file_name +'</a></div>'
                            }
                        }
                    }
                    $('#chatContext').append( htmlText );
                });
            });
            $('#contextModal').modal('show');
        }

        function loadFile(){
            $('#chat').removeClass();
            $('#milestone').removeClass();
            $('#task').removeClass();
            $('#update').removeClass();
            $('#file').addClass('active');
            $('#frame').text(" ");
            var text='';
            <?php if(empty($files)){?>
            text = ' <h4>Sorry, there is no file matches your search term...</h4>';
            <?php }else{?>
            text=' <table id="data" class="table table-striped"> <thead><th>Project Code</th> <th>File</th> <th>Time modified</th></thead>';
            <?php foreach($files as $f):?>
            text=text+'<tr><td>'+ '<?=$f['project_code']?>'+'</td> <td>'+'<a href="'+ '<?=$f['file_url']?>' +'" target="blank">'+ '<?=addslashes($f['file_key'])?>' +'</a></td> <td>'+
            '<?=$f['last_updated']?>' +'</td></tr>';
            <?php endforeach ?>
            text = text +'</table>';
            <?php }?>
            $('#frame').append( text );
            $('#data').dataTable(
                {
                    "bFilter": false,
                    "bInfo": false,
                    "bLengthChange": false,
                    "pageLength": 15
                }
            );

        }
        $(document).ready(function(){
            $('#data').dataTable(
                {
                    "bFilter": false,
                    "bInfo": false,
                    "bLengthChange": false,
                    "pageLength": 15
                }
            );
        });
        </script>
</html>