<?php

/**
 * Created by PhpStorm.
 * User: yuanyuxuan
 * Date: 3/16/16
 * Time: 2:04 PM
 */
class Project_build
{
    public function all_ongoing_with_score(){
        $CI =& get_instance();
        $CI->load->model("Project_model");
        $projects = $CI->Project_model->retrieve_all_ongoing();
        $threshold = $this->get_threshold();
        for ($x = 0; $x < count($projects); $x++) {
            $score = $this->get_issue_urgency_score($projects[$x]['project_id']);
            //$score = $this->get_issue_urgency_score($projects[$x]['project_id']);
            $projects[$x]['score']=$score;
            if($score>$threshold['upper']){
                $projects[$x]['level']="High";
            }elseif($score<=$threshold['lower']){
                $projects[$x]['level']="Low";
            }else{
                $projects[$x]['level']="Moderate";
            }
        }
        //sort($projects, "score");
        function cmp($a, $b)
        {
            return $a["score"]<$b["score"];
        }
        usort($projects, "cmp");
        return $projects;
    }

    public function get_level($score){
        $threshold = $this->get_threshold();
        if($score>$threshold['upper']){
            return "High";
        }elseif($score<=$threshold['lower']){
            return "Low";
        }else{
            return "Moderate";
        }
    }

    public function get_issue_urgency_score($project_id){
        $CI =& get_instance();
        $CI->load->model("Issue_report_model");
        $issues= $CI->Issue_report_model->get_ongoing_issue_per_project($project_id);
        $sum = 0;
        //calculate the urgency for each pending issue
        //var_dump($issues);
        foreach($issues as $value){
            if(isset($value["date_due"]) && $value["date_due"]!= "0000-00-00"){
                $dates_left=$value["days_to_go"];
                if($dates_left<0){
                    $dates_left=1;
                }
                $score = (sqrt($value["issue_pr"])/($dates_left+1))*sqrt($value["proj_pr"]);
                $sum += $score;
            }
        }
        return round($sum, 2);
    }

    public function get_threshold(){
        $CI =& get_instance();
        $CI->load->model("Project_model");
        $projects = $CI->Project_model->retrieve_all_ongoing();
        $arr = array();
        for ($x = 0; $x < count($projects); $x++) {
            array_push($arr,$this->get_issue_urgency_score($projects[$x]['project_id']));
        }
        $mean = array_sum($arr)/count($arr);
        $fVariance = 0.0;
        foreach ($arr as $i)
        {
            $fVariance += pow($i - $mean, 2);
        }
        $fVariance /= ( false ? count($arr) - 1 : count($arr) );
        $sdv = (float) sqrt($fVariance);

        $upper = $mean + $sdv;
        $lower = (float)0;
        if($sdv<$mean){
            $lower = $mean-$sdv;
        }
        return array("upper"=>$upper,"lower"=>$lower);
    }
}