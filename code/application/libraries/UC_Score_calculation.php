<?php

/**
 * Created by PhpStorm.
 * User: yuanyuxuan
 * Date: 3/31/16
 * Time: 10:09 AM
 */
class UC_Score_calculation
{
    public function TCF($project_id){
        $CI =& get_instance();
        $CI->load->model("Project_model");
        $project = $CI->Project_model->retrieve_by_id($project_id);
        //var_dump($project);
        $TF = (int)$project["distributed_system"]*2
            +(int)$project["response_time"]
            +(int)$project["internal_processing_complexity"]
            +(int)$project["code_reusability"]
            +(int)$project["easy_to_install"]*0.5
            +(int)$project["easy_to_use"]*0.5
            +(int)$project["portability_to_other_platforms"]*2
            +(int)$project["system_maintenance"]
            +(int)$project["concurrent_processing"]
            +(int)$project["security_features"]
            +(int)$project["access_for_third_parties"]
            +(int)$project["end_user_training"];
        //var_dump($TF);
        $TCF = 0.6 + ($TF/100);
        return $TCF;
    }

    public function UUCW($project_id){
        $CI =& get_instance();
        $CI->load->model("Use_case_model");
        $uc = $CI->Use_case_model->retrieve_by_project_id($project_id);
        $score = 0;
        //var_dump($uc[1]["classification"]);
        //var_dump(($uc[1]["classification"]=="Simple"));
        foreach ($uc as $u){
            if($u["classification"]=="Simple"){
                $score+=5;
            }elseif($u["classification"]=="Average"){
                $score+=10;
            }else{
                $score+=15;
            }
        }
        return $score;
    }

    public function UAW($project_id){
        $CI =& get_instance();
        $CI->load->model("Actor_model");
        $actor = $CI->Actor_model->retrieve_by_project_id($project_id);

        //var_dump($actor);
        $score = 0;
        //var_dump($uc[1]["classification"]);
        //var_dump(($uc[1]["classification"]=="Simple"));
        foreach ($actor as $a){
            if($a["classification"]=="Simple"){
                $score+=1;
            }elseif($a["classification"]=="Average"){
                $score+=2;
            }else{
                $score+=3;
            }
        }
        return $score;

    }

    public function ECF(){
        $CI =& get_instance();
        $CI->load->model("Environment_factor_model");
        $environment = $CI->Environment_factor_model->retrieve();
        //var_dump($environment);

        $EF = 0;
        //var_dump($uc[1]["classification"]);
        //var_dump(($uc[1]["classification"]=="Simple"));
        foreach ($environment as $e){
            $EF+= $e["Weight"]*$e["assigned_value"];
        }
        $ECF = 1.4 -0.03*$EF;
        return $ECF;
    }

    public function UCP($project_id){
        $UUCW = $this->UUCW($project_id);
        $UAW = $this->UAW($project_id);
        $TCF = $this->TCF($project_id);
        $ECF = $this->ECF();
        $UCP = ($UUCW+$UAW)*$TCF*$ECF;
        return $UCP;
    }

    public function getWorkinghour(){
        $CI =& get_instance();
        $CI->load->model("Environment_factor_model");
        $CI->load->model("Internal_user_model");
        $devlopers = $CI->Internal_user_model->retrieve_by_type("Developer");
        $environment = $CI->Environment_factor_model->working_hour();
        $working_hour = $environment[0]['assigned_value'];
        $num_worker = count($devlopers);
        return $num_worker*$working_hour;
    }

    public function get_estimate_deadline($project_id){
        $new_plan = $this->get_plan($project_id);
        end($new_plan);
        $recommended_deadline = key($new_plan);
        $stop_date = date("Y-m-d", strtotime($recommended_deadline));
        return $stop_date;
    }

    public function get_plan($project_id){
        $CI =& get_instance();
        $CI->load->model("Schedule_model");
        $workinghour = $this->getWorkinghour();
        $UCP = $this->UCP($project_id);
        $total_time = $UCP*$_SERVER["DEV_HOUR_PER_UCP"];
        $current_plan = $CI->Schedule_model->retrieve();

        $new_plan = Array();
        //$last_date = date("d-m-Y");
        $current_date = date("Ymd",strtotime(date("Ymd") . "+1 days"));


        foreach($current_plan as $cp){
            $date_in = str_replace('-', '', $cp['date_time']);
            while($current_date!=$date_in){
                $new_plan[$current_date] = min($total_time,$workinghour);
                $total_time = $total_time - $new_plan[$current_date];
                if($total_time<=0){
                    break 2;
                }
                $current_date = date("Ymd",strtotime($current_date . "+1 days"));
            }
            if((int)$cp['hours_planned']<(int)$workinghour){
                $new_plan[$current_date] = min($total_time,$workinghour-(int)$cp['hours_planned']);
                $total_time = $total_time - $new_plan[$current_date];
                if($total_time<=0){
                    break 1;
                }
                $current_date = date("Ymd",strtotime($current_date . "+1 days"));
            }
        }

        while(true){
            $new_plan[$current_date] = min($total_time,$workinghour);
            $total_time = $total_time - $new_plan[$current_date];
            if($total_time<=0){
                break 1;
            }
            $current_date = date("Ymd",strtotime($current_date . "+1 days"));
        }
        return $new_plan;
    }

    public function make_plan($project_id, $set_deadline, $given_deadline){
        $new_plan = $this -> get_plan($project_id);
        $total_time = array_sum($new_plan);
        $text = str_replace('-', '', $set_deadline);
        $text2 = str_replace('-', '', $given_deadline);
        $current_date = date("Ymd",strtotime(date("Ymd") . "+1 days"));
        $deadline = date("Ymd",strtotime($text));
        $compute = date("Ymd",strtotime($text2));
        //var_dump($new_plan);
        $diff = abs(strtotime($deadline) - strtotime($current_date));
        $total_days = floor($diff/ (60*60*24));
        //var_dump($total_time);
        $average = $total_time/$total_days;

        $diff2 = abs(strtotime($deadline) - strtotime($compute));
        $extra_days = (int)floor($diff2/ (60*60*24));

        $diff3 = abs(strtotime($compute) - strtotime($current_date));
        $planed_days = (int)floor($diff3/ (60*60*24));
        //var_dump($new_plan);
        if($set_deadline<$given_deadline){
            $cumm = 0;
            $date_for_plan = $compute;
            for ($x = 0; $x < $extra_days; $x++) {
                $cumm += $new_plan[$date_for_plan];
                $new_plan[$date_for_plan] = 0;
                $date_for_plan = date("Ymd",strtotime($date_for_plan . "-1 days"));
            }
            $cumm = array_sum($new_plan);
            $left = $total_time-$cumm;
            //var_dump($left);
            $average2 = $left/$total_days;
            //var_dump($average2);
            $date_for_plan = $current_date;
            for($x = 0; $x < $total_days; $x++){
                $date_for_plan = date("Ymd",strtotime($date_for_plan . "+1 days"));
                $new_plan[$date_for_plan] = $new_plan[$date_for_plan]+$average2;
            }
            $cumm = array_sum($new_plan);
            $left = $total_time-$cumm;
            //var_dump($left);
            //var_dump($new_plan);
            return $new_plan;

        }else{
            $date_for_plan = $compute;
            for ($x = 0; $x < $extra_days; $x++) {
                $date_for_plan = date("Ymd",strtotime($date_for_plan . "+1 days"));
                //var_dump($date_for_plan);
                $new_plan[$date_for_plan] = $average;
            }
            $date_for_plan = $current_date;
            for($x = 0; $x <= $planed_days; $x++){
                $new_plan[$date_for_plan] = min($average, $new_plan[$date_for_plan]);
                $date_for_plan = date("Ymd",strtotime($date_for_plan . "+1 days"));
            }
            //var_dump($new_plan);
            $cumm = array_sum($new_plan);
            $left = $total_time-$cumm;
            $average2 = $left/$total_days;
            $date_for_plan = $current_date;
            for($x = 0; $x <= $total_days; $x++){
                $new_plan[$date_for_plan] = $new_plan[$date_for_plan]+$average2;
                $date_for_plan = date("Ymd",strtotime($date_for_plan . "+1 days"));
            }
            return $new_plan;
        }
    }
}