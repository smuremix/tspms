<?php

/**
 * Created by PhpStorm.
 * User: yuanyuxuan
 * Date: 3/31/16
 * Time: 7:44 AM
 */


class Environment_factor_model extends CI_Model
{
    public function __construct(){
        parent::__construct();
        $this->load->helper("date");
    }
    public function retrieve(){
        $query = $this->db->query("SELECT * FROM environmental_factor");
        return $query->result_array();
    }

    public function working_hour(){
        $query = $this->db->query("SELECT * FROM environmental_factor where type = 'Schedule'");
        return $query->result_array();
    }

    /**
     * @param $update_array including all attributes including u_id
     * @return affected rows
     */
    public function update($update_array){
        $date = new DateTime("now",new DateTimeZone(DATETIMEZONE));
        $update_array['last_updated'] = $date->format('c');
        $this->db->update('internal_user', $update_array, array('u_id' => $update_array['u_id']));
        return $this->db->affected_rows();
    }

    public function edit_env_fact($insert_array){
        $keys = array_keys($insert_array);
        for ($x = 0; $x < sizeof($insert_array); $x++) {
            $key = $keys[$x];
            $this->db->where('Description', $key);
            $to_insert = array('assigned_value'=>$insert_array[$key]);
            $this->db->update('environmental_factor', $to_insert);
        }
        return $this->db->affected_rows();
    }

}