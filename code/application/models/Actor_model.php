<?php
/**
 * Created by PhpStorm.
 * User: yuanyuxuan
 * Date: 3/30/16
 * Time: 2:02 PM
 */

class Actor_model extends CI_Model{

    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
        $this->load->helper("date");
    }

    public function insert($insert_array){
        $date = new DateTime("now",new DateTimeZone(DATETIMEZONE));
        $insert_array['last_updated'] = $date->format('c');
        $this->db->insert('actor', $insert_array);
        $actor_id = $this->db->insert_id();
        return $actor_id;
    }

    public function retrieveAll(){
        $query = $this->db->query("select * from actor");
        return $query->result_array();
    }

    public function get_sub_id($project_id){
        $query = $this->db->query("select max(sub_id)as id from actor where project_id=".$project_id);
        return $query->row_array()['id'];
    }

    public function get_no_of_actor_by_project($project_id){
        $query = $this->db->query("select count(*)as number from actor where project_id=".$project_id);
        return $query->row_array()['number'];
    }

    public function retrieve_by_id($actor_id){
        if(isset($actor_id)){
            $query = $this->db->get_where("actor",["actor_id"=>$actor_id]);
            if( $query->num_rows()>0){
                return $query->row_array();
            }
        }
        return null;
    }
    public function retrieve_by_project_id($p_id){
        if(isset($p_id)){
            $query = $this->db->get_where("actor",["project_id"=>$p_id]);
            //$this->db->order_by("sub_id", "asc");
            return $query->result_array();

        }
        return null;
    }


    public function update($update_array){
        //$date = date('Y-m-d H:i:s');
        $date = new DateTime("now",new DateTimeZone(DATETIMEZONE));
        $update_array['last_updated'] =  $date->format('c');
        $this->db->update('actor', $update_array, array('actor_id' => $update_array['actor_id']));
        return $this->db->affected_rows();
    }

    public function delete_actor($actor_id){
        if(isset($actor_id)){
            $this->db->delete('actor', array('actor_id' => $actor_id));
        }
        return null;
    }
}