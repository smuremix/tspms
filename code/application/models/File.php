<?php
class File extends CI_Model {

    function get_by_fid($fid=FALSE){
        $query = $this->db->get_where('upload', array('fid' => $fid));
        return $query->row_array();
    }

    function insert($file_to_upload=FALSE){
        if($file_to_upload){
            $temp_array = array(
                'file_url'=>$file_to_upload['file_url'],
                'file_key'=>$file_to_upload['file_key'],
                'filename'=>$file_to_upload['filename'],
                'file_type'=>$file_to_upload['file_type'],
                'parent_id'=>$file_to_upload['parent_id'],
                'index'=>$file_to_upload['index'],
                'pid'=>$file_to_upload['pid'],
                'file_size'=>$file_to_upload['file_size']
            );

            $now = new DateTime("now", new DateTimeZone(DATETIMEZONE));
            $this->db->set('last_updated', $now->format('c'));
            $this->db->insert('upload', $temp_array);
            return $this->db->insert_id();
        }else{
            return FALSE;
        }
    }

    function delete_by_fid($fid=FALSE){
        if($fid){
            $this->db->delete('upload', array('fid' => $fid));
            return $this->db->affected_rows();
        }else{
            return 0;
        }
    }

    function retrieveAll($project_id){
        if($project_id) {
            $query = $this->db->get_where("upload",['pid' => $project_id]);
        }
        return $query->result_array();
    }

    function retrieve_all_ordered_by_name($project_id){
        if($project_id) {
            $query = $this->db->order_by('filename', 'ASC')->get_where("upload",['pid' => $project_id]);
        }
        return $query->result_array();
    }

    function rename_by_fid($update_array){
        $date = new DateTime("now",new DateTimeZone(DATETIMEZONE));
        $update_array['last_updated'] = $date->format('c');
        $this->db->update('upload', $update_array, array('fid' => $update_array['fid']));
        return $this->db->affected_rows();
    }

    function get_all_children($fid){
        $query = $this->db->get_where('upload', array('parent_id' => $fid));
        return $query->result_array();
    }

    function search_file($search_term)
    {
        $search_term="%".$search_term."%";

        $sql = "SELECT upload.*,project.project_code from upload, project where upload.pid=project.project_id
      and file_key LIKE ? order by project_id
      ";

        $query=$this->db->query($sql,
            array(
                $search_term,
            )
        );

        return $query->result_array();
    }
}