<?php

/**
 * Created by PhpStorm.
 * User: yuanyuxuan
 * Date: 4/7/16
 * Time: 10:16 AM
 */


class Schedule_model extends CI_Model
{
    public function __construct(){
        parent::__construct();
        $this->load->helper("date");
    }
    public function retrieve(){
        $query = $this->db->query("select date_time , sum(hour_planned) as hours_planned from schedule where date_time > now() group by date_time order by date_time;");
        return $query->result_array();
    }

    public function retrieve_by_time($start_date, $end_date){
        $query = $this->db->query("select date_time , sum(hour_planned) as hours_planned from schedule where date_time between $start_date and $end_date group by date_time;");
        return $query->result_array();
    }

    /**
     * @param $update_array including all attributes including u_id
     * @return affected rows
     */
    /**
    public function update($update_array){
        $date = new DateTime("now",new DateTimeZone(DATETIMEZONE));
        $update_array['last_updated'] = $date->format('c');
        $this->db->update('internal_user', $update_array, array('u_id' => $update_array['u_id']));
        return $this->db->affected_rows();
    }
    **/

    public function insert($insert_array){
        $this->db->insert_batch('schedule', $insert_array);
    }

    public function erase($project_id){
        $this->db->delete('schedule', array('project_id' => $project_id));
    }

}