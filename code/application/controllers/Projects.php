<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Project
 *
 * @author WANG Tiantong
 */

class Projects extends CI_Controller {
    //put your code here
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->model("Project_model");
        $this->load->model("Chat_model");
        $this->load->model("Customer_model");
        $this->load->model("Internal_user_model");
        $this->load->model("Project_phase_model");
        $this->load->model("Milestone_model");
        $this->load->model("Update_model");
        $this->load->model("Phase_model");
        $this->load->model("Environment_factor_model");
        $this->load->model("Task_model");
        $this->load->model("Use_case_model");
        $this->load->model("Notification_model");
        $this->load->model("Schedule_model");
        $this->load->model("Internal_user_model");
        $this->load->library('Project_build');
        $this->load->library('BB_issues');
        $this->load->helper('auth');

    }

    public function index(){
        redirect('list_all/');
    }

    public function list_all(){
        authenticate("p");
        $this->load->library('Project_build');
        $projects = $this->project_build->all_ongoing_with_score();
        //$projects = $this->all_ongoing_with_score();
        $this->load->view('project/pm_all_ongoing_projects',$data=array('projects'=>$projects));
    }

    public function uc_point($project_id){
        authenticate("p");
        $this->load->library('UC_Score_calculation');
        $UCP = $this->uc_score_calculation->UCP($project_id);
        $time_per_point = 25;
        $total_time = $UCP*25;
        echo "Use Case Point:";
        echo $UCP;
        echo "|";
        echo "Developing Hours each UCP: ";
        echo $time_per_point;
        echo "|";
        echo "Developing Hours for the project: ";
        echo $total_time;
    }

    public function estimate_deadline($project_id){
        authenticate("p");
        $this->load->library('UC_Score_calculation');

        /**
        $workinghour = $this->uc_score_calculation->getWorkinghour();
        $UCP = $this->uc_score_calculation->UCP($project_id);
        $total_time = $UCP*25;
        $this->load->library('schedule_model');
        $current_plan = $this->Schedule_model->retrieve();


        $new_plan = Array();
        //$last_date = date("d-m-Y");
        $current_date = date("Ymd",strtotime(date("Ymd") . "+1 days"));


        foreach($current_plan as $cp){
            $date_in = str_replace('-', '', $cp['date_time']);
            while($current_date!=$date_in){
                $new_plan[$current_date] = min($total_time,$workinghour);
                $total_time = $total_time - $new_plan[$current_date];
                if($total_time<=0){
                    break 2;
                }
                $current_date = date("Ymd",strtotime($current_date . "+1 days"));
            }
            if((int)$cp['hours_planned']<(int)$workinghour){
                $new_plan[$current_date] = min($total_time,$workinghour-(int)$cp['hours_planned']);
                $total_time = $total_time - $new_plan[$current_date];
                if($total_time<=0){
                    break 1;
                }
                $current_date = date("Ymd",strtotime($current_date . "+1 days"));
            }
        }

        while(true){
            $new_plan[$current_date] = min($total_time,$workinghour);
            $total_time = $total_time - $new_plan[$current_date];
            if($total_time<=0){
                break 1;
            }
            $current_date = date("Ymd",strtotime($current_date . "+1 days"));
        }
        end($new_plan);
        $recommended_deadline = key($new_plan);
        $stop_date = date("Y-m-d", strtotime($recommended_deadline));
         **/
        $stop_date = $this ->uc_score_calculation->get_estimate_deadline($project_id);
        echo $stop_date;
    }

    public function list_past_projects(){
        authenticate("p");
        $projects = $this->Project_model->retrieve_all_past();
        $this->load->view('project/pm_all_past_project',$data=array('projects'=>$projects));
    }
    public function insert($insert_array){
        authenticate("p");
        $this->Project_model->insert($insert_array);
        $new_project_id = $this->db->insert_id();
        $session_uid = $this->session->userdata('internal_uid');
        $created_by = $this->Internal_user_model->retrieve_name($session_uid);
        $change_type = "New Project Created";
        $redirect = "view_dashboard";
        $users = $this->Internal_user_model->retrieve_all_pm();
        $this->Notification_model->add_new_project_notifications($new_project_id,$change_type,$created_by,$redirect,$users);
        return $new_project_id;
        //$this->Project_phase_model->create_phases_upon_new_project($project_id);
    }
    public function ajax_retrieve_all_tags(){
        echo json_encode($this->Project_model->getTags());
    }

    public function create_new_project() {
        authenticate("p");
        $this->load->library('form_validation');
        $this->form_validation->set_rules('c_username', 'Customer Username', 'is_unique[customer.username]');
        if ($this->form_validation->run()) {
            $customer_option = $this->input->post("customer_option");
            $c_id = '';
            if ($customer_option == "from-existing") {
                $c_id = $this->input->post("c_id");
            } else {
                $new_customer = array(
                    'title' => $this->input->post("title"),
                    'first_name' => $this->input->post("first_name"),
                    'last_name' => $this->input->post("last_name"),
                    'company_name' => $this->input->post("company_name"),
                    'email' => $this->input->post("email"),
                    'hp_number' => $this->input->post("hp_number"),
                    'other_number' => $this->input->post("other_number"),
                    'username' => $this->input->post("c_username"),
                    'password_hash' => password_hash($this->input->post('password'), PASSWORD_DEFAULT)
                );
                $c_id = $this->Customer_model->insert($new_customer);
                $this->Chat_model->online_add('USER', $c_id);
                if ($c_id < 0) {
                    $this->session->set_userdata('message', 'Cannot create new project,please contact administrator.');
                    $this->load->view('project/pm_project_new', $data = ["customers" => $this->Customer_model->retrieveAll()]);
                }
            }
            //$current_project_phase_id = $this->Project_phase_model->retrieve_last_project_phase_id()+1;
            $insert_array = array(
                'c_id' => $c_id,
                'project_title' => $this->input->post("project_title"),
                'project_code' => $this->input->post("project_code"),
                'project_description' => $this->input->post("project_description"),
                'tags' => $this->input->post("tags"),
                'remarks' => $this->input->post("remarks"),
                'file_repo_name' => $this->input->post("file_repo_name"),
                'staging_link' => $this->input->post("staging_link"),
                'production_link' => $this->input->post("production_link"),
                'customer_preview_link' => $this->input->post("customer_preview_link"),
                'bitbucket_repo_name' => $this->input->post("bitbucket_repo_name"),
                'project_value' => $this->input->post("project_value"),
                'priority' => $this->input->post("priority"),
                'current_project_phase_id' => 0,
                'pm_id'=>$this->input->post("pm_id")
            );
            $initial_chat = [
                "customer_id" => $c_id,
                "pm_id" => $this->input->post("pm_id"),
                "body" => "Hi, I am the project manager for your project [".$this->input->post("project_title")."]. Please contact me if you have any problem."
            ];
            //convert bb repo name to lower case
            $insert_array["bitbucket_repo_name"] = strtolower($insert_array["bitbucket_repo_name"]);
            //update if the given repo name is valid
            $this->load->library("BB_shared");
            $repo_name_valid = $this->bb_shared->validate_repo_name_with_bb($insert_array["bitbucket_repo_name"]);
            $insert_array["repo_name_valid"] = $repo_name_valid?1:0;

            if ($this->insert($insert_array)) {
                if($this->Chat_model->initialize_new($initial_chat)>0) {
                    $this->session->set_userdata('message', 'New project and chat thread has been created successfully.');
                }else{
                    $this->session->set_userdata('message', 'New project and  has been created successfully.Error when creating chat thread.');
                }
                redirect('Projects/list_all');
            } else {
                $this->session->set_userdata('message', 'Cannot create new project,please contact administrator.');
                $this->load->view('project/pm_project_new', $data = ["customers" => $this->Customer_model->retrieveAll(),
                    "pms"=>$this->Internal_user_model->retrieve_by_type("PM")

                ]);
            }

        } else {
            $this->load->view('project/pm_project_new', $data = ["customers" => $this->Customer_model->retrieveAll(),
                "pms"=>$this->Internal_user_model->retrieve_by_type("PM")
            ]);
        }

    }

    public function edit($project_id=null){
        if(!isset($project_id)) {show_404();die();}
        authenticate("p");
        $this->load->library('form_validation');
        $this->load->view('project/pm_project_edit',
            $data = ["project" => $this->Project_model->retrieve_by_id($project_id),
                "customers" => $this->Customer_model->retrieveAll(),
                "phases" => $this->Project_phase_model->retrievePhaseDef(),
                "pms" => $this->Internal_user_model->retrieve_by_type("PM")
            ]);
    }
    /*changed function name to process_edit*/


    public function process_edit($project_id=null){
        if(!isset($project_id)) {show_404();die();}
        authenticate("p");
        $has_error = false;
        $this->load->library('form_validation');
        $this->form_validation->set_rules('username', 'Customer Username', 'is_unique[customer.username]');
        if ($this->form_validation->run()) {
            $original_array = $this->Project_model->retrieve_by_id($project_id);
            $name_array = ["c_id", "project_title"
                , "project_description", "tags", "remarks"
                , "file_repo_name", "priority"
                , "bitbucket_repo_name", "project_value", "staging_link", "production_link", "customer_preview_link","pm_id", "project_code"];
            $input = $this->input->post($name_array, true);
            $customer_option = $this->input->post('customer_option');

            if ($customer_option == 'from-existing') {
                $input['c_id'] = $this->input->post('c_id');
                if($original_array['c_id']!== $this->input->post('c_id')){
                    $initial_chat = [
                        "customer_id" => $this->input->post('c_id'),
                        "pm_id" => $this->input->post("pm_id"),
                        "body" => "Hi, I am the project manager for your project [".$this->input->post("project_title")."]. Please contact me if you have any problem."
                    ];
                    $this->Chat_model->initialize_new($initial_chat);
                }
            } else {//create new customer
                $customer_name_array = ["title", "first_name"
                    , "last_name", "company_name", "hp_number"
                    , "other_number", "email", "username", "password_hash"];

                $new_customer_input = $this->input->post($customer_name_array, true);
                $new_customer_input['password_hash'] = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
                $new_customer_id = $this->Customer_model->insert($new_customer_input);
                $this->Chat_model->online_add('USER', $new_customer_id);
                if ($new_customer_id == false) {
                    echo "something wrong happen when creating customer";
                } else {
                    $input['c_id'] = $new_customer_id;
                    $initial_chat = [
                        "customer_id" => $new_customer_id,
                        "pm_id" => $this->input->post("pm_id"),
                        "body" => "Hi, I am the project manager for your project [".$this->input->post("project_title")."]. Please contact me if you have any problem."
                    ];
                    $this->Chat_model->initialize_new($initial_chat);
                }

            }
            //convert bb repo name to lower case
            $input["bitbucket_repo_name"] = strtolower($input["bitbucket_repo_name"]);
            //update if the given repo name is valid
            $this->load->library("BB_shared");
            $repo_name_valid = $this->bb_shared->validate_repo_name_with_bb($input["bitbucket_repo_name"]);
            //var_dump($input["bitbucket_repo_name"]);
            //var_dump($repo_name_valid);
            $input["repo_name_valid"] = $repo_name_valid?1:0;
            if(!$repo_name_valid){
                $this->session->set_userdata('message', 'The Bibucket repo name is invalid. Please check it again');
                $has_error = true;
            }

            foreach ($input as $key => $value) {
                if ($value !== null) {
                    $original_array[$key] = $value;
                }
            }

            if ($this->Project_model->update($original_array) == 1) {

                if(!$has_error)$this->session->set_userdata('message', 'Project has been edited successfully.');
                $session_uid = $this->session->userdata('internal_uid');
                $created_by = $this->Internal_user_model->retrieve_name($session_uid);
                $change_type = "Project Details Edited";
                $redirect = "view_dashboard";
                $users = $this->Internal_user_model->retrieve_all_pm();
                $this->Notification_model->add_new_project_notifications($project_id,$change_type,$created_by,$redirect,$users);
                redirect('projects/view_dashboard/'.$project_id);
            }else{
                $this->session->set_userdata('message', 'Cannot edit project,please contact administrator.');
                $this->edit($project_id);
            }

        }
    }

    public function retrieveDataForProjectUpdatePage($project_id=null){
        if(!isset($project_id)) {show_404();die();}
        authenticate("p");
        //phase
        $project = $this->Project_model->retrieve_by_id($project_id);
        $current_project_phase_id = $project['current_project_phase_id'];
        $current_project_phase = $this->Project_phase_model->retrieve_by_id($current_project_phase_id);
        $next_phase_id = $current_project_phase['phase_id']+1;
        $next_phase =  $this->Phase_model->retrieve_phase_by_id($next_phase_id);
        $next_phase_name = $next_phase['phase_name'];
        if($current_project_phase_id==0){
            $next_phase_name = "Lead";
        }
        $phases=$this->Project_phase_model->retrieve_by_project_id($project_id);

        //milestones
        $milestones = $this->Milestone_model-> retrieve_by_project_phase_id($project['current_project_phase_id']);

        //updates
        $updates = $this->Update_model-> retrieve_by_project_phase_id($project['current_project_phase_id']);

        $data = [
            "project"=>$project,
            "phases"=>$phases,
            "milestones"=>$milestones,
            "updates"=>$updates,
            "next_phase_name"=>$next_phase_name
        ];
        return $data;
    }

    public function view_dashboard($project_id=null){
        if(!isset($project_id)) {show_404();die();}
        authenticate("p");
        $this->load->library('Project_build');
        $score = $this->project_build->get_issue_urgency_score($project_id);
        //phase
        $project = $this->Project_model->retrieve_by_id($project_id);
        $phases=$this->Project_phase_model->retrieve_by_project_id($project_id);
        $tasks = $this->Task_model->retrieve_all_uncompleted_by_project_id($project_id);
        if(intval($project['current_project_phase_id'])===0){
            $current_phase_name = "Lead";
        }else{
            if(intval($project['current_project_phase_id'])===-1){
                $current_phase_name = 'Ended';
            }else{
                $current_phase_name = $this->Project_phase_model->retrieve_phase_name_by_id($project['current_project_phase_id']);
                $current_phase_name = $current_phase_name[0]['phase_name'];
            }
        }
        $newTasks = array();
        foreach($tasks as $t){
            $days_left = $this->Task_model->get_days_left($t['task_id']);
            $t['days_left'] = $days_left;
            array_push($newTasks,$t);
        }
        //customer_name
        $c_id = $project['c_id'];
        $customer = $this->Customer_model->retrieve($c_id);
       $no_of_usecases = $this->Use_case_model->get_no_of_usecase_by_project($project_id);

        $data = [
            "project"=>$project,
            "phases"=>$phases,
            "customer"=>$customer,
            "tasks"=>$newTasks,
            "current_phase_name"=>$current_phase_name,
            "no_of_usecases"=>$no_of_usecases,
            "score"=>$score
        ];
        $this->load->view('project/pm_project_dashboard',$data);

        //$this->load->view('project/project_update',$data=["project"=>$project,"current_phase"=>$current_phase,"current_project_phase_id"=>$current_project_phase_id]);
    }

    public function view_report($project_id=null){
        if(!isset($project_id)) {show_404();die();}
        authenticate("p");
        $this->load->library('Project_build');
        $score = $this->project_build->get_issue_urgency_score($project_id);
        $level = $this->project_build->get_level($score);
        //phase
        $project = $this->Project_model->retrieve_by_id($project_id);
        $phases=$this->Project_phase_model->retrieve_by_project_id($project_id);
        $this->project_build->get_threshold();
        $newTasks = array();

        $data = [
            "project"=>$project,
            "score"=>$score,
            "level"=>$level
        ];
        $this->load->view('project/project_report',$data);

        //$this->load->view('project/project_update',$data=["project"=>$project,"current_phase"=>$current_phase,"current_project_phase_id"=>$current_project_phase_id]);
    }

    public function view_updates($project_id=null){
        if(!isset($project_id)) {show_404();die();}
        authenticate("p");
        $data = $this->retrieveDataForProjectUpdatePage($project_id);
        $this->load->view('project/pm_project_update',$data);
    }

    public function customer_overview($c_id=null){
        if(!isset($c_id)) {show_404();die();}
        $customer_project = $this->Project_model->retrieve_by_c_id($c_id);
        authenticate("c");
        if($customer_project){
            //customer has more than one project
            if(sizeof($customer_project)>1){
                $this->load->view('project/customer_project_list',$data=["projects"=>$customer_project]);
            }else{
                $this->customer_view($customer_project[0]['project_id']);
            }
        }else{
            $this->session->set_userdata('message','This account has no project associated with us. please check with administrator. ');
            redirect('/customer_authentication/login/');
        }
    }

    public function customer_view($project_id=null){
        if(!isset($project_id)) {$this->load->view("errors/html/error_404");die();}
        authenticate("c");
        $project = $this->Project_model->retrieve_by_id($project_id);
        if($project){
            $data=array("project"=>$project,
                "phases"=>$this->Project_phase_model->retrieve_by_project_id($project['project_id']),
                "updates"=>$this->Update_model->retrieve_by_project_phase_id($project['current_project_phase_id']),
                "milestones"=>$this->Milestone_model->retrieve_by_project_phase_id($project['current_project_phase_id']),
                "project_id"=>$project_id
            );
            $this->load->view('project/customer_project_dashboard',$data);
        }

    }

    public function dev_page(){
        authenticate("d");
        $projects=$this->Project_model->retrieve_all_ongoing();
        $no_of_issues=[];
        foreach($projects as $p){
            if($p['bitbucket_repo_name']!= null) {
                if(isset($this->bb_issues->retrieveIssues($p['bitbucket_repo_name'],null)['count'])) {
                    $no_of_issues[$p['project_id']] =$this->bb_issues->retrieveIssues($p['bitbucket_repo_name'],null)['count'] ;
                }
            }
        }
        $data["projects"]=$projects;
        $data["no_of_issues"]=$no_of_issues;
        $this->load->view('project/developer_dashboard',$data);//to add developer page

    }

    public function bb_repo_name_ajax(){
        $repo_name =  $this->input->get("repo_name");
        $repo_id =  $this->input->get("repo_id");
        $this->load->library("BB_shared");
        $result =  $this->bb_shared->validate_repo_name_with_bb($repo_name,$repo_id);
        echo $result? "true":"false";
    }

    public function edit_tech_factor($project_id){
        $insert_task_array['distributed_system']=$this->input->post("distributed_system");
        $insert_task_array['end_user_efficiency']=$this->input->post("end_user_efficiency");
        $insert_task_array['internal_processing_complexity']=$this->input->post("internal_processing_complexity");
        $insert_task_array['code_reusability']=$this->input->post("code_reusability");
        $insert_task_array['easy_to_install']=$this->input->post("easy_to_install");
        $insert_task_array['easy_to_use']=$this->input->post("easy_to_use");
        $insert_task_array['portability_to_other_platforms']=$this->input->post("portability_to_other_platforms");
        $insert_task_array['system_maintenance']=$this->input->post("system_maintenance");
        $insert_task_array['concurrent_processing']=$this->input->post("concurrent_processings");
        $insert_task_array['security_features']=$this->input->post("security_features");
        $insert_task_array['access_for_third_parties']=$this->input->post("access_for_third_parties");
        $insert_task_array['end_user_training']=$this->input->post("end_user_training");
        $insert_task_array['response_time']=$this->input->post("response_time");

        if($this->Project_model->edit_tech_fact($insert_task_array, $project_id)==1){
            redirect('Usecases/list_all/'.$project_id);
        }

    }

}
