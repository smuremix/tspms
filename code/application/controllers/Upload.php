<?php

require 'vendor/autoload.php';
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

class Upload extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->model('File');
        $this->load->model('Project_model');
        $this->load->helper('auth');
    }

    public function index(){
    }

    public function upload($project_id){
        authenticate("p");
        $project = $this->Project_model->retrieve_by_id($project_id);
        $this->load->view('file_repo/upload_view', $data = ['project' => $project]);
    }

    public function customer_repo($project_id){
        authenticate("c");
        $project = $this->Project_model->retrieve_by_id($project_id);
        $this->load->view('file_repo/customer_repo', $data = ['project' => $project]);
    }

    public function file_upload($project_id){
        if($file_to_upload=$this->upload_file_to_s3($project_id)){
            if($fid=$this->File->insert($file_to_upload)){
                $json_response = array(
                    'message'=>'Upload successful',
                    'status'=>'success'
                );
                $this->output->set_content_type('application/json')->set_output(json_encode($json_response));
            }else{
                $json_response = array(
                    'message'=>'Save to DB failed.',
                    'status'=>'error'
                );
                $this->output->set_content_type('application/json')->set_output(json_encode($json_response));
            }

        }else{
            $json_response = array(
                'message'=>'Upload to S3 failed.',
                'status'=>'error'
            );
            $this->output->set_content_type('application/json')->set_output(json_encode($json_response));
        }
    }

    private function upload_file_to_s3($project_id){
        $s3 = new S3Client([
            'credentials' => [
                'key'    => $_SERVER["S3_KEY"],
                'secret' => $_SERVER["S3_SECRET"]
            ],
            'version' => '2006-03-01',
            'region'  => 'ap-southeast-1'
        ]);

        try {
            $file_to_upload=array();
            $file_to_upload['filename'] = $_FILES['file_to_upload']['name'];
            $file_to_upload['file_key'] = substr(md5(time().$file_to_upload['filename']),0,5)."_".$file_to_upload['filename'];
            $result = $s3 -> putObject(
                array(
                    'Bucket'       => 'tspms',
                    'Key'          => $project_id.'/'.$file_to_upload['file_key'],
                    'SourceFile'   => $_FILES['file_to_upload']['tmp_name'],
                    'ContentType'  => $_FILES['file_to_upload']['type'],
                    'ACL'          => 'public-read',
                    'Metadata'     => array(
                        'filename' => $_FILES['file_to_upload']['name']
                    )
                )
            );

            $file_to_upload['file_type'] = 'default';
            switch (strrchr($file_to_upload['filename'], '.')) {
                case '.png':
                case '.jpg':
                case '.jpeg':
                case '.tif':
                case '.tiff':
                case '.gif':
                    $file_to_upload['file_type'] = 'image';
                    break;
                case '.pdf':
                    $file_to_upload['file_type'] = 'pdf';
                    break;
                case '.doc':
                case '.docx':
                case ".pages":
                    $file_to_upload['file_type'] = 'word';
                    break;
                case '.ppt':
                case '.pptx':
                case '.pps':
                case '.ppsx':
                case ".key":
                    $file_to_upload['file_type'] = 'ppt';
                    break;
                case '.zip':
                case '.rar':
                    $file_to_upload['file_type'] = 'archive';
                    break;
                case '.xlsx':
                case '.xlsb':
                case '..xltx':
                case '.xls':
                case '.xlt':
                    $file_to_upload['file_type'] = 'excel';
                    break;
                case '.mkv':
                case '.flv':
                case '.avi':
                case '.mov':
                case '.wmv':
                case '.rm':
                case '.rmvb':
                case '.mpg':
                case '.mp4':
                    $file_to_upload['file_type'] = 'video';
                    break;
                case '.aac':
                case '.m4a':
                case '.m4b':
                case '.mp3':
                case '.wav':
                case '.wma':
                case ".iff":
                case ".m3u":
                    $file_to_upload['file_type'] = 'audio';
                    break;
                case '.txt':
                case ".log":
                case ".csv":
                case ".xml":
                    $file_to_upload['file_type'] = 'text';
                    break;
                case ".java":
                case ".js":
                case ".php":
                case ".css":
                case ".html":
                case ".py":
                case ".bat":
                case ".cpp":
                case ".tex":
                case ".sql":
                case ".swift":
                    $file_to_upload['file_type'] = 'code';
                    break;
                default:
                    break;
            }

            $file_to_upload['filename'] = substr($file_to_upload['filename'], 0, strpos($file_to_upload['filename'], "."));
            $file_to_upload['file_url'] = $result['ObjectURL'];
            $file_to_upload['parent_id'] = 0;
            $file_to_upload['pid'] = $project_id;
            $file_to_upload['file_size'] = $_FILES['file_to_upload']['size'];

            $file_to_upload['index'] = 0;
            $file_list=$this->File->retrieveAll($project_id);
            foreach($file_list as $value){
                if((int)$value['index']>=(int)$file_to_upload['index']){
                    $file_to_upload['index'] = (int)$value['index']+1;
                }

                if($value['filename'] == $file_to_upload['filename']){
                    $file_to_upload['filename'] = $file_to_upload['filename']." copy";
                }
            }

            return $file_to_upload;
        } catch (S3Exception $e) {
            return FALSE;
        }
    }

    public function delete_by_fid($fid){
        $file_to_delete=$this->File->get_by_fid($fid);
        if($file_to_delete){
            if($file_to_delete['file_type'] != 'folder') {
                $s3 = new S3Client([
                    'credentials' => [
                        'key' => 'AKIAJCISFJKSJ7DGAM5A',
                        'secret' => '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'
                    ],
                    'version' => '2006-03-01',
                    'region' => 'ap-southeast-1'
                ]);

                $s3->deleteObject(
                    array(
                        'Bucket' => 'test-upload-file',
                        'Key' => $file_to_delete['pid'] . '/' . $file_to_delete['file_key']
                    )
                );

                $this->File->delete_by_fid($fid);
            } else {
                $children_details=$this->File->get_all_children($fid);
                $this->File->delete_by_fid($fid);
                foreach($children_details as $value){
                    $this->delete_by_fid($value['fid']);
                }
            }


            $json_response = array(
                'message'=>'Delete request received.',
                'status'=>'success'
            );

            $this->output->set_content_type('application/json')->set_output(json_encode($json_response));
        }else{
            $json_response = array(
                'message'=>'Invalid file id',
                'status'=>'error'
            );

            $this->output->set_content_type('application/json')->set_output(json_encode($json_response));
        }
    }

    public function get_by_fid($fid){
        $file=$this->File->get_by_fid($fid);
        $this->output->set_content_type('application/json')->set_output(json_encode($file));
    }

    public function get_all_files($project_id){
        $file_list=$this->File->retrieveAll($project_id);
        $project=$this->Project_model->retrieve_by_id($project_id);

        $file_array=array();
        foreach($file_list as $value){
            $file=array();
            $file['id'] = $value['fid'];
            $file['parent'] = $value['parent_id'];
            $file['text'] = $value['filename'];
            $file['type'] = $value['file_type'];
            $file['file_size'] = $value['file_size'];
            $file['a_attr'] = array(
              'href' => $value['file_url']
            );
            array_push($file_array, $file);
        }

        $parent=array();
        $parent['id'] = '0';
        $parent['parent'] = '#';
        $parent['text'] = $project['project_title'];
        $parent['type'] = 'folder';
        $parent['state'] = array (
            'opened'=>true
        );
        array_push($file_array, $parent);

        $this->output->set_content_type('application/json')->set_output(json_encode($file_array));
    }

    public function rename_file($project_id){
        $fid=$this->input->post('fid');
        $name=$this->input->post('new_name');
        $file=$this->File->get_by_fid($fid);

        $file_list=$this->File->retrieve_all_ordered_by_name($project_id);
        foreach($file_list as $value){
            if($value['filename'] == $name){
                $name = $name." copy";
            }
        }

        if($file['filename'] != $name){
            $updated_file=array(
                'fid'=>$fid,
                'file_url'=>$file['file_url'],
                'file_key'=>$file['file_key'],
                'filename'=>$name,
                'last_updated'=>$file['last_updated'],
                'file_type'=>$file['file_type'],
                'parent_id'=>$file['parent_id'],
                'index'=>$file['index'],
                'pid'=>$file['pid'],
                'file_size'=>$file['file_size']
            );

            $this->File->rename_by_fid($updated_file);
        }
    }

    public function move_file(){
        $fid=$this->input->post('fid');
        $new_parent_id=$this->input->post('new_parent_id');
        $new_index=$this->input->post('new_index');
        $file=$this->File->get_by_fid($fid);

        if($file['parent_id'] != $new_parent_id){
            $updated_file=array(
                'fid'=>$fid,
                'file_url'=>$file['file_url'],
                'file_key'=>$file['file_key'],
                'filename'=>$file['filename'],
                'last_updated'=>$file['last_updated'],
                'pid'=>$file['pid'],
                'file_type'=>$file['file_type'],
                'parent_id'=>$new_parent_id,
                'index'=>$new_index,
                'file_size'=>$file['file_size']
            );

            $this->File->rename_by_fid($updated_file);
        }
    }

    public function create_folder($project_id){
        $new_folder=array();
        $new_folder['filename'] = $this->input->post('folder_name');
        $new_folder['file_key'] = "";
        $new_folder['file_url'] = "";
        $new_folder['file_type'] = 'folder';
        $new_folder['parent_id'] = 0;
        $new_folder['file_size'] = 0;

        $new_folder['index'] = 0;
        $file_list=$this->File->retrieve_all_ordered_by_name($project_id);
        foreach($file_list as $value){
            if((int)$value['index'] >= (int)$new_folder['index']){
                $new_folder['index'] = (int)$value['index']+1;
            }

            if($value['filename'] == $new_folder['filename']){
                $new_folder['filename'] = $new_folder['filename']." copy";
            }
        }

        $new_folder['pid'] = $project_id;

        if($fid=$this->File->insert($new_folder)){
            $json_response = array(
                'message'=>'Folder creation successful',
                'status'=>'success'
            );
            $this->output->set_content_type('application/json')->set_output(json_encode($json_response));
        }
    }
}