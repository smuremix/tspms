<?php

class Search extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->model("Task_model");
        $this->load->model("Milestone_model");
        $this->load->model("Chat_model");
        $this->load->model("Update_model");
        $this->load->model("File");
    }
    public function index(){
        $key = $this->input->get('key');
        $tasks = $this->Task_model->search_task($key);
        $updates = $this->Update_model->search_update($key);
        $milestones = $this->Milestone_model->search_milestone($key);
        $chats = $this->Chat_model->search_chat($key, $this->session->userdata('internal_uid'));
        $files = $this->File->search_file($key);
        $highlight = '<span style="background-color: #FFFF00">'.$key.'</span>';
        foreach($tasks as &$v){
            $v['content']=str_ireplace($key,$highlight,$v['content']);
            //var_dump($t);
        }
        foreach($chats as &$v){
            $v['body']=str_ireplace($key,$highlight,$v['body']);
            $v['file_name']=str_ireplace($key,$highlight,$v['file_name']);
        }
        foreach($files as &$v){
            $v['file_key']=str_ireplace($key,$highlight,$v['file_key']);
        }
        foreach($updates as &$v){
            $v['body']=str_ireplace($key,$highlight,$v['body']);
            $v['header']=str_ireplace($key,$highlight,$v['header']);
        }
        foreach($milestones as &$v){
            $v['body']=str_ireplace($key,$highlight,$v['body']);
            $v['header']=str_ireplace($key,$highlight,$v['header']);
        }
        unset($v);
        $data=array(
            'tasks'=>$tasks,
            'milestones'=>$milestones,
            'updates'=>$updates,
            'chats'=>$chats,
            'files'=>$files,
            'key'=>$key
        );
        $this->load->view('search/search_result',$data);

    }



}