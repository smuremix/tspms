<?php
/**
 * Created by PhpStorm.
 * User: 123
 * Date: 2016-01-01
 * Time: 9:40 PM
 */
class Usecases extends CI_Controller {

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->helper("date");
        $this->load->model("Use_case_model");
        $this->load->model("Project_model");
        $this->load->model("Actor_model");
        $this->load->helper('auth');

    }

    public function list_all($project_id){
        authenticate("pd");
        $usecases = $this->Use_case_model->retrieve_by_project_id($project_id);
        $actor = $this->Actor_model->retrieve_by_project_id($project_id);

        $this->load->view('project/usecases', $data=[
            "project" => $this->Project_model->retrieve_by_id($project_id),
            "usecases"=>$usecases,
            "actor"=>$actor,

        ]);

    }

    public function customer_usecases($project_id){
        authenticate("c");
        $usecases = $this->Use_case_model->retrieve_external_by_project_id($project_id);

        $this->load->view('project/customer_usecases', $data=[
            "project" => $this->Project_model->retrieve_by_id($project_id),
            "usecases"=> $usecases,
            "project_id"=>$project_id
        ]);
    }

    public function delete_usecase($uc_id){
        authenticate("p");
        $usecase = $this->Use_case_model->retrieve_by_id($uc_id);
        $this->Use_case_model->delete_usecase($uc_id);
        $this->session->set_userdata('message', 'Use Case deleted successfully.');
        redirect('Usecases/list_all/'.$usecase['project_id']);

    }

    public function delete_actor($actor_id){
        authenticate("p");
        $actor = $this->Actor_model->retrieve_by_id($actor_id);
        $this->Actor_model->delete_actor($actor_id);
        $this->session->set_userdata('message', 'Use Case deleted successfully.');
        redirect('Usecases/list_all/'.$actor['project_id']);

    }

    public function new_use_case($project_id){
        authenticate("p");
        if($this->input->post("submit")) {
            $insert_array = array(
                'project_id' => $project_id,
                'sub_id' => ($this->Use_case_model->get_sub_id($project_id) + 1),
                'title' => $this->input->post("title"),
                'flow' => $this->input->post("flow"),
                'classification' => $this->input->post("classification"),
                'stakeholders' => $this->input->post("stakeholders"),
                'type' => $this->input->post("type"),
            );
            if ($this->Use_case_model->insert($insert_array) > 0) {
                $this->session->set_userdata('message', 'New use case has been created successfully.');
                redirect('usecases/list_all/' . $project_id);
            } else {
                $this->session->set_userdata('message', 'Cannot create new usecase,please contact administrator.');
                $this->load->view('project/use_case_new', $data = ["project" => $this->Project_model->retrieve_by_id($project_id)]);
            }
        }else{
            $this->load->view('project/use_case_new', $data = ["project" => $this->Project_model->retrieve_by_id($project_id)]);
        }
    }

    public function new_actor($project_id){
        authenticate("p");
        if($this->input->post("submit")) {
            $insert_array = array(
                'project_id' => $project_id,
                'sub_id' => ($this->Actor_model->get_sub_id($project_id) + 1),
                'actor_name' => $this->input->post("actor_name"),
                'description' => $this->input->post("description"),
                'classification' => $this->input->post("classification"),
            );
            //var_dump($insert_array);
            if ($this->Actor_model->insert($insert_array) > 0) {
                $this->session->set_userdata('message', 'New actor has been created successfully.');
                redirect('usecases/list_all/' . $project_id);
            } else {
                $this->session->set_userdata('message', 'Cannot create new actor,please contact administrator.');
                $this->load->view('project/actor_new', $data = ["project" => $this->Project_model->retrieve_by_id($project_id)]);
            }
        }else{
            $this->load->view('project/actor_new', $data = ["project" => $this->Project_model->retrieve_by_id($project_id)]);
        }
    }

    public function edit_use_case($uc_id){
        authenticate("p");
        $usecase = $this->Use_case_model->retrieve_by_id($uc_id);
        if ($this->input->post("submit")) {
            $usecase['title'] = $this->input->post("title");
            $usecase['flow'] = $this->input->post("flow");
            $usecase['classification'] = $this->input->post("classification");
            $usecase['stakeholders'] = $this->input->post("stakeholders");
            $usecase['type'] = $this->input->post("type");

            if ($this->Use_case_model->update($usecase) > 0) {
                $this->session->set_userdata('message', 'Use case has been changed successfully.');
                redirect('usecases/list_all/' . $usecase['project_id']);
            } else {
                $this->session->set_userdata('message', 'Cannot edit usecase,please contact administrator.');
                $this->load->view('project/use_case_edit', $data = ["usecase" => $usecase,
                    "project" => $this->Project_model->retrieve_by_id($usecase['project_id'])
                ]);
            }
        } else {
            $this->load->view('project/use_case_edit',$data = ["usecase" => $usecase,
                "project" => $this->Project_model->retrieve_by_id($usecase['project_id'])
            ]);
        }
    }
    public function edit_actor($actor_id){
        authenticate("p");
        $actor = $this->Actor_model->retrieve_by_id($actor_id);
        if ($this->input->post("submit")) {
            $actor['actor_name'] = $this->input->post("actor_name");
            $actor['description'] = $this->input->post("description");
            $actor['classification'] = $this->input->post("classification");

            if ($this->Actor_model->update($actor) > 0) {
                $this->session->set_userdata('message', 'Actor has been changed successfully.');
                redirect('usecases/list_all/' . $actor['project_id']);
            } else {
                $this->session->set_userdata('message', 'Cannot edit usecase,please contact administrator.');
                $this->load->view('project/edit_actor', $data = ["usecase" => $actor,
                    "project" => $this->Project_model->retrieve_by_id($actor['project_id'])
                ]);
            }
        } else {
            $this->load->view('project/edit_actor',$data = ["actor" => $actor,
                "project" => $this->Project_model->retrieve_by_id($actor['project_id'])
            ]);
        }
    }
}