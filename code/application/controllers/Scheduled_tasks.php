<?php

/**
 * Interface for scheduler. Functions here are not expected to render view and are thus unnecessary for authentication
 */
class Scheduled_tasks extends CI_Controller{
    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->library('BB_issues');
        $this->load->library('BB_scheduled_tasks');
        $this->load->model('Internal_user_model');
        $this->load->model('Issue_report_model');
        $this->session->userdata('internal_uid',1);
    }
    /* Calibrates bb milestones with database
     * prints {"added":num_added,"removed":num_removed,"time":time_taken}
     */
    function _is_authenticated(){
        $authenticated = false;
        if($this->session->userdata('internal_uid')&&$this->session->userdata('internal_type')=="PM"){
            $authenticated = true;
        }else{
            $all_pm_records = $this->Internal_user_model->retrieve_all_pm();
            if(isset($all_pm_records)&&isset($all_pm_records[0])&&!empty($all_pm_records[0]["bb_username"])){
                $pm_id = $all_pm_records[0]["u_id"];
                $this->session->set_userdata('internal_uid',$pm_id);
                $authenticated = true;
            }else{
                //die("No project manager found");
            }
        }
        return $authenticated;
    }
    function calibrate_bb_milestones(){
        $data = [];
        if($this->_is_authenticated()){
            $this->load->library("BB_scheduled_tasks");
            $result = $this->bb_scheduled_tasks->calibrate_bb_milestones();
            $data["result"] = $result;
        }else{
            $data["error"] = "Authentication error";
            $this->output->set_status_header('401');
        }
        echo json_encode($data);

    }

    /**
     * This method can be called by scheduler and project list page(through ajax call)
     */
    public function fetch_issue_counts(){
        $this->load->library("BB_scheduled_tasks");
        $result = $this->bb_scheduled_tasks->fetch_issue_counts();
        echo json_encode($result);
    }
    /**
     * This method can be called by scheduler and project list page(through ajax call)
     */
    public function fetch_outstanding_issue_counts(){
        $this->load->library("BB_scheduled_tasks");
        $result = $this->bb_scheduled_tasks->fetch_outstanding_issue_counts();
        echo json_encode($result);
    }
    public function get_project_urgency_score($project_id){
        $this->load->model("Issue_report_model");
        $issues= $this->Issue_report_model->get_ongoing_issue_per_project($project_id);
        $sum = 0;
        //calculate the urgency for each pending issue
        //var_dump($issues);
        foreach($issues as $value){
            if(isset($value["date_due"]) && $value["date_due"]!= "0000-00-00"){
                $dates_left=$value["days_to_go"];
                if($dates_left<0){
                    $dates_left=1;
                }
                $score = (sqrt($value["issue_pr"])/($dates_left+1))*sqrt($value["proj_pr"]);
                $sum += $score;
            }
        }
        if($sum==0){
            echo 0;
        }else{
            echo number_format($sum, 2);
        }
    }

    public function get_issue_urgency_score_across_projects(){
        $this->load->model("Issue_report_model");
        $issues= $this->Issue_report_model->get_ongoing_issue_across_projects();
        $sum = 0;
        //calculate the urgency for each pending issue
        //var_dump($issues);
        foreach($issues as $value){
            if(isset($value["date_due"]) && $value["date_due"]!= "0000-00-00"){
                $dates_left=$value["days_to_go"];
                if($dates_left<0){
                    $dates_left=1;
                }
                $score = (sqrt($value["issue_pr"])/($dates_left+1))*sqrt($value["proj_pr"]);
                $sum += $score;
            }
        }
        if($sum==0){
            echo 0;
        }else{
            echo number_format($sum, 2);
        }
    }

    public function store_issue_urgency_score_across_projects(){
        $this->load->model("Issue_report_model");
        $issues= $this->Issue_report_model->get_ongoing_issue_across_projects();
        $sum = 0;
        //calculate the urgency for each pending issue
        //var_dump($issues);
        foreach($issues as $value){
            if(isset($value["date_due"]) && $value["date_due"]!= "0000-00-00"){
                $dates_left=$value["days_to_go"];
                if($dates_left<0){
                    $dates_left=1;
                }
                $score = (sqrt($value["issue_pr"])/($dates_left+1))*sqrt($value["proj_pr"]);
                $sum += $score;
            }
        }
        $this->Issue_report_model->insert_total_urgency_score($sum);
    }

    public function retrieve_urgency_score(){
        $this->load->model("Issue_report_model");
        $records= $this->Issue_report_model->retrieve_report_model();
        //var_dump($records);

        $table = array();
        $table['cols'] = array(

            // Labels for your chart, these represent the column titles
            // Note that one column is in "string" format and another one is in "number" format as pie chart only required "numbers" for calculating percentage and string will be used for column title
            array('label' => 'Time', 'type' => 'string'),
            array('label' => 'Urgency Score', 'type' => 'number'),
        );

        $rows = [];
        foreach($records as $v){

            $date = substr($v['time'], 5,5);
            $row = [
                "c"=>[
                    ['v' => (string) $date],
                    ['v' => (float) $v['score']]
                ]
            ];
            array_push($rows, $row);
        }

        $table['rows'] = $rows;
        $jsonTable = json_encode($table);
        echo $jsonTable;
    }



}