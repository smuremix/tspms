<?php

/**
 * Created by PhpStorm.
 * User: WANG Tiantong
 * Date: 10/6/2015
 * Time: 10:23 PM
 */
class Project_phase extends CI_Controller{

    public function __construct(){
        parent::__construct();
        // Your own constructor code
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->model("Project_phase_model");
        $this->load->model("Project_model");
        $this->load->model("Phase_model");
        $this->load->model("Internal_user_model");
        $this->load->model("Notification_model");
        $this->load->model("Schedule_model");
    }
    public function update_est_end_date($project_id,$current_project_phase_id){
            $update_array_1 = $this->Project_phase_model->retrieve_by_id($current_project_phase_id);
            $estimated_end_time = $this->input->post("new_estimated_end_date");
            $estimated_end_time = new DateTime($estimated_end_time);
            $update_array['estimated_end_time'] = $estimated_end_time->format('c');
            $current_phase_id = $update_array_1['phase_id'];
            if($current_phase_id == 3){
                $this->load->library('UC_Score_calculation');
                $deadline_given_by_computer = $this ->uc_score_calculation->get_estimate_deadline($project_id);
                if($deadline_given_by_computer == $estimated_end_time){
                    $insert_schedule = $this ->uc_score_calculation->get_plan($project_id);
                }else {
                    $insert_schedule = $this->uc_score_calculation->make_plan($project_id, $estimated_end_time, $deadline_given_by_computer);
                }

                $keys = array_keys($insert_schedule);
                $update_schedule = Array();
                for ($x = 0; $x < sizeof($insert_schedule); $x++) {
                    $key = $keys[$x];
                    $date_to_insert = date("Y-m-d", strtotime($key));
                    $record=Array('project_id' => $project_id, 'date_time' => $date_to_insert, 'hour_planned'=>$insert_schedule[$key]);
                    array_push($update_schedule,$record);
                }
                $this->Schedule_model->erase($project_id);
                $this->Schedule_model->insert($update_schedule);
            }
            if($this->Project_phase_model->edit($update_array)){
                $this->session->set_userdata('message', 'Phase estimated end date updated successfully.');
            }else{
                $this->session->set_userdata('message', 'An error occurred, please contact administrator.');
            }
            redirect('projects/view_updates/'.$project_id);
    }

    public function update_phase($project_id,$current_project_phase_id){
        if($this->session->userdata('internal_uid')&&$this->session->userdata('internal_type')=="PM") {
            if(!$current_project_phase_id==0) {
                $update_array = $this->Project_phase_model->retrieve_by_id($current_project_phase_id);
                $this->Project_phase_model->update($update_array);
                $current_phase_id = $update_array['phase_id'];

                $next_phase_id = $current_phase_id + 1;
                $insert_array['project_id'] = $project_id;
                $insert_array['phase_id'] = $next_phase_id;
                $insert_array['end_time'] = null;

                $estimated_end_time = $this->input->post("estimated_end_time");
                if($current_phase_id == 2){
                    $this->load->library('UC_Score_calculation');
                    $deadline_given_by_computer = $this ->uc_score_calculation->get_estimate_deadline($project_id);
                    if($deadline_given_by_computer == $estimated_end_time){
                        $insert_schedule = $this ->uc_score_calculation->get_plan($project_id);
                    }else {
                        $insert_schedule = $this->uc_score_calculation->make_plan($project_id, $estimated_end_time, $deadline_given_by_computer);
                    }

                    $keys = array_keys($insert_schedule);
                    $update_schedule = Array();
                    for ($x = 0; $x < sizeof($insert_schedule); $x++) {
                        $key = $keys[$x];
                        $date_to_insert = date("Y-m-d", strtotime($key));
                        $record=Array('project_id' => $project_id, 'date_time' => $date_to_insert, 'hour_planned'=>$insert_schedule[$key]);
                        array_push($update_schedule,$record);
                    }
                    $this->Schedule_model->insert($update_schedule);

                }
                $estimated_end_time = new DateTime($estimated_end_time);
                $insert_array['estimated_end_time'] = $estimated_end_time->format('c');

                $next_project_phase_id = $this->Project_phase_model->insert($insert_array);
                $update_array_project = $this->Project_model->retrieve_by_id($project_id);
                $update_array_project['current_project_phase_id'] = $next_project_phase_id;

                if($this->Project_model->update($update_array_project)==1){
                    $session_uid = $this->session->userdata('internal_uid');
                    $created_by = $this->Internal_user_model->retrieve_name($session_uid);
                    $change_type = "Project Phase Updated";
                    $redirect = "view_dashboard";
                    $users = $this->Internal_user_model->retrieve_all_pm();
                    $this->Notification_model->add_new_project_notifications($project_id,$change_type,$created_by,$redirect,$users);
                    redirect('projects/view_updates/'.$project_id);
                }

            }else{
                $insert_array['project_id'] = $project_id;
                $insert_array['phase_id'] = 1;
                $insert_array['end_time'] = null;
                $insert_array['estimated_end_time'] = $this->input->post("estimated_end_time");
                $next_project_phase_id = $this->Project_phase_model->insert($insert_array);
                $update_array_project = $this->Project_model->retrieve_by_id($project_id);
                $update_array_project['current_project_phase_id'] = $next_project_phase_id;
                if($this->Project_model->update($update_array_project)==1){
                    $session_uid = $this->session->userdata('internal_uid');
                    $created_by = $this->Internal_user_model->retrieve_name($session_uid);
                    $change_type = "Project Started";
                    $redirect = "view_dashboard";
                    $users = $this->Internal_user_model->retrieve_all_pm();
                    $this->Notification_model->add_new_project_notifications($project_id,$change_type,$created_by,$redirect,$users);
                    redirect('projects/view_updates/'.$project_id);
                }

            }
        }else{
            $this->session->set_userdata('message','You have not login / have no access rights. ');
            redirect('/internal_authentication/login/');
        }
    }

    public function end_project($project_id,$current_project_phase_id){
        if($this->session->userdata('internal_uid')&&$this->session->userdata('internal_type')=="PM") {
            $update_array = $this->Project_phase_model->retrieve_by_id($current_project_phase_id);
            $this->Project_phase_model->update($update_array);

            $update_array_project = $this->Project_model->retrieve_by_id($project_id);
            $update_array_project['current_project_phase_id'] = -1;
            $update_array_project['is_ongoing'] = 0;

            if($this->Project_model->update($update_array_project)==1){
                $session_uid = $this->session->userdata('internal_uid');
                $created_by = $this->Internal_user_model->retrieve_name($session_uid);
                $change_type = "Project Ended";
                $redirect = "view_dashboard";
                $users = $this->Internal_user_model->retrieve_all_pm();
                $this->Notification_model->add_new_project_notifications($project_id,$change_type,$created_by,$redirect,$users);
                redirect('projects/list_all');
            }
        }else{
            $this->session->set_userdata('message','You have not login / have no access rights. ');
            redirect('/internal_authentication/login/');
        }
    }


}