<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if ( ! function_exists('authenticate')){
    function authenticate($allowed_users, $auth_page = NULL, $redirect_back = TRUE) {
        $is_authenticated = false;
        $ci =&get_instance();
        //$ci->session->unset_userdata("after_login");
        $internal_auth_page = "/internal_authentication/login";
        $customer_auth_page = "/customer_authentication/login";
        //var_dump($ci->session->userdata('internal_type'));
        //if allows pm
        //var_dump($allowed_users);
        if(strpos($allowed_users,"p")!==false && !$is_authenticated){
            //if logged in as pm
            if($ci->session->userdata('internal_uid')&&$ci->session->userdata('internal_type')=="PM") {
                $is_authenticated = true;
            }else {
                $auth_page = $internal_auth_page;
            }
        }
        if(strpos($allowed_users,"d")!==false &&!$is_authenticated){
            if ($ci->session->userdata('internal_uid')&&
              $ci->session->userdata('internal_type')=="Developer") {
                $is_authenticated = true;
            }else {
                $auth_page = $internal_auth_page;
            }
        }
        if(strpos($allowed_users,"c")!==false && !$is_authenticated){
            if ($ci->session->userdata('Customer_username')) {
                $is_authenticated = true;
            }else {
                $auth_page = $customer_auth_page;
            }
        }
        if (!$is_authenticated && $auth_page){
            $ci->session->set_userdata('message','Please login.');
            $url = $auth_page;
            if ($redirect_back) $url .= "?f=".base64_encode($_SERVER['REQUEST_URI']);
            redirect($url);
        }

    }
}
